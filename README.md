<a href="https://repology.org/project/gk6x-rs/versions">
    <img src="https://repology.org/badge/vertical-allrepos/gk6x-rs.svg" alt="Packaging Status" align="right">
</a>

[![pipeline status](https://gitlab.com/OSS-keyboards/gk6x-rs/badges/domina/pipeline.svg)](https://gitlab.com/OSS-keyboards/gk6x-rs/-/commits/domina)
[![dependency status](https://deps.rs/repo/gitlab/foss-drivers/gk6x-rs/status.svg)](https://deps.rs/repo/gitlab/foss-drivers/gk6x-rs)
[![](https://tokei.rs/b1/gitlab/OSS-keyboards/gk6x-rs)](https://gitlab.com/OSS-keyboards/gk6x-rs)

GX6X RS
================



Description
-----------

FOSS Driver written in Rust for keyboard PCB's of type (GK64, GK84, GK61, etc).

Warning!
I worked on this really only when I was depressed and tried to distract myself from reality, so the code is looking that way, PR's are welcome!


[Darwin | Original Driver](http://www.jikedingzhi.com/downloadlist?driverID=90)

[Windows | Original Driver](http://www.jikedingzhi.com/downloadlist?driverID=41)

Goal
----

Full support of Lightning, Macros and Binding/Set keys.
To have same or more functions/features than the original drivers.
At least that's the plan.

Downloads
---------

**Releases** - The binary can be downloaded at [Release Page](https://gitlab.com/OSS-keyboards/gk6x-rs/-/releases).

**Source** - The source can be downloaded as a [.compressed archive](https://gitlab.com/OSS-keyboards/gk6x-rs/-/archive/domina/gk6x-rs-domina.zip), or cloned from our [GitLab Repo](https://gitlab.com/OSS-keyboards/gk6x-rs).


"Manual" Installation (Not Finished)
---------------------
(If possible use your favorite package manager!)

Requirements:
- rustup with updated nightly toolchain


Git clone or download though other means from repo [^Downloads](#Downloads)

**Linux**

- Go to a folder named "installer"
- Run with sudo file named "install.sh"

**Darwin**

TODO: Add Darwin installation process

**Windows**

TODO: Add Windows installation process


Usage
-----

On Linux, you have to run it as a root user or with sudo. A normal user doesn't have access to open usb devices. ¯\\\_(ツ)_/¯
In future in installation process it will add expections/permission for the current user to access specific usb devices.
The [rules file](installer/linux/51-gk6x.rules) should be put in "/etc/udev/rules.d/".

Project Members
---------------

- [Arisa Snowbell](https://gitlab.com/Arisa_Snowbell) - Maintainer/Architect/Developer
- [Unknown](https://gitlab.com/) - GUI Designer

Thanks to | Related Projects
----------------------------


[GK6X](https://github.com/pixeltris/GK6X)

[GK64 Firmware disasm](https://github.com/wgwoods/gk64-python)

[GK6X GUI](https://github.com/ukeloop/gk6x_gui)

[USRS XPAD](https://gitlab.com/zskamljic/usrs-xpad/)

License
-------
The license is `GNU General Public License v3.0` (May change at release, FIXME: Before public release)
