use super::bithelper::Byte;

#[inline]
fn crc_alg(crc: &mut u16, b: Byte) {
	/* Fetch byte from memory, XOR into CRC top byte*/
	*crc ^= u16::from(b) << 8;

	/* Prepare to rotate 8 bits */
	(0..8).for_each(|_| {
		/* b15 is set... */
		if (*crc & 0x8000) == 0x0000 {
			/* b15 is clear... */
			/* Just rotate */
			*crc <<= 1;
		} else {
			/* b15 is not clear... */
			/* Rotate and XOR with XMODEM polynomic */
			*crc = (*crc << 1) ^ 0x1021; // POLY number should be 0x11021 by the site where they wrote it in C++ but this works too
		}
	}); /* Iter over 8 bits */
}

pub fn get_crc(data: &[Byte], index: Option<usize>, crc: Option<u16>) -> u16 {
	let mut crc = crc.unwrap_or(0xFFFF);
	let index = index.unwrap_or(0);

	// Step through bytes in memory
	// Iter over all bytes from specified index in the slice to the end of the slice
	data[index..].iter().for_each(|b| crc_alg(&mut crc, *b));

	/* Return updated CRC */
	crc
}

pub fn get_crc_byte(b: Byte, crc: Option<u16>) -> u16 {
	let mut crc = crc.unwrap_or(0xFFFF);

	crc_alg(&mut crc, b);

	/* Return updated CRC */
	crc
}

pub fn insert_crc(data: &mut [Byte], data_offset: Option<usize>, crc_offset: Option<usize>) {
	let data_offset = data_offset.unwrap_or(0);
	let crc_offset = crc_offset.unwrap_or(6);

	data[crc_offset..=crc_offset + 1].copy_from_slice(&[0, 0]);
	let data_crc = get_crc(data, Some(data_offset), None);
	data[crc_offset..=crc_offset + 1].copy_from_slice(&data_crc.to_le_bytes());
}

pub fn validate_crc(data: &mut [Byte], data_offset: Option<usize>, crc_offset: Option<usize>) -> bool {
	let data_offset = data_offset.unwrap_or(0);
	let crc_offset = crc_offset.unwrap_or(6);

	let temp = &mut [0; 2];
	data[crc_offset..=crc_offset + 1].swap_with_slice(temp);
	let calculated_crc = get_crc(data, Some(data_offset), None);
	data[crc_offset..=crc_offset + 1].copy_from_slice(temp);
	u16::from_le_bytes(*temp) == calculated_crc
}
