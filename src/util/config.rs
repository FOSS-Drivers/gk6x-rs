use std::{
	fs::OpenOptions,
	io::{Read, Write},
};

use anyhow::Result;
use once_cell::sync::OnceCell;
use serde::{Deserialize, Serialize};
use tracing::{debug, error};

use crate::util::localization::Language;

use super::CONFIG_PATH;

static LOADED_CONFIG: OnceCell<Config> = OnceCell::new();

pub const DEFAULT_CONFIG: Config = Config { language: Language::English };

#[derive(Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct Config {
	pub language: Language,
}

impl Drop for Config {
	fn drop(&mut self) {
		if *self == DEFAULT_CONFIG {
			debug!("There is no need to save the config because it was not changed!");
			return;
		}

		match LOADED_CONFIG.get() {
			Some(config) => {
				if self == config {
					debug!("There is no need to save the config because it was not changed!");
					return;
				}
			}
			None => error!("Failed to load first loaded version of the config! Saving the config for safety!"),
		}

		match self.save() {
			Ok(_) => debug!("The config was successfully saved!"),
			Err(e) => error!("Failed to save the config on drop of the config because \"{}\"!", e),
		};
	}
}

impl Config {
	pub fn save(&mut self) -> Result<()> {
		let mut file = OpenOptions::new().write(true).create(true).open(&*CONFIG_PATH)?;
		file.write_all(serde_json::to_string_pretty(&self)?.as_bytes())?;
		Ok(())
	}

	pub fn load() -> Result<Self> {
		Ok(match OpenOptions::new().read(true).open(&*CONFIG_PATH) {
			Ok(mut file) => {
				let s = file.metadata().map(|m| m.len() as usize + 1).unwrap_or(0);
				let mut contents = String::with_capacity(s);
				file.read_to_string(&mut contents)?;
				let v: Self = serde_json::from_str(&contents)?;
				if LOADED_CONFIG.set(v.clone()).is_err() {
					error!("Failed to set loaded config into global variable first version of the config!");
				}
				v
			}
			Err(e) => {
				error!("Failed to load settings file because \"{}\"!", e);
				DEFAULT_CONFIG
			}
		})
	}
}
