use std::fs;
use std::io::prelude::*;
use std::{
	collections::HashMap,
	ffi::OsStr,
	fs::{DirBuilder, OpenOptions},
	io::Cursor,
	path::PathBuf,
};

use anyhow::{bail, Result};
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use chrono::Utc;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use tracing::debug;

use crate::uni::Error;

use super::{bithelper::Byte, crc16};

// These values need to be correct as they form part of the crc calculation
#[repr(u64)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum CMFileType {
	/// 0=??? some chinese characters? (CE DE D0 A7 00)
	Invalid = 0,
	/// PROFILE
	Profile = 1,
	/// LIGHT
	Light = 2,
	/// STATASTIC
	Statastic = 3,
	/// APPCONF
	AppConf = 4,
	/// MACRO
	Macro = 5,
}

impl CMFileType {
	// I hope this is a joke lolz
	const fn as_bytes(&self) -> &'static [u8] {
		match self {
			Self::Invalid => &[0xCE, 0xDE, 0xD0, 0xA7], // Chinese Simplified letters? "ÎÞÐ§"="0xCE, 0xDE, 0xD0, 0xA7"="无效"="Invalid", the chinese letters are translated to "invalid" or "null"
			Self::Profile => b"PROFILE",
			Self::Light => b"LIGHT",
			Self::Statastic => b"STATASTIC",
			Self::AppConf => b"APPCONF",
			Self::Macro => b"MACRO",
		}
	}
}

/// Magic / signature "1FMC"
const FILE_SIGNATURE: u32 = 0x434D4631;

macro_rules! hashmap {
    ($($k:expr => $v:expr),* $(,)?) => {
        std::iter::Iterator::collect(std::array::IntoIter::new([$(($k, $v),)*]))
    };
}

lazy_static! {

	// This is a hack... these values are wack, no idea what's going on with them the f*ck
	// FIXME: Figure this out + verify the numbers
	static ref UNKNOWN_MAPS: HashMap<u32, u16> = hashmap![
		/*u32::from_le_bytes(*b"ihds")*/ 0x73646869 => 25869,
		/*u32::from_le_bytes(*b"IHDS")*/ 0x53444849 => 3155,
		0xA7D0DECE => 36218 // 4 bytes in BigEndian order represented in number, but it is the "Invalid" byte sequence when converted to LittleEndian
	];
}

pub fn load(path: PathBuf) -> Result<Vec<Byte>> {
	let mut file = OpenOptions::new().read(true).open(&path)?;
	let s = file.metadata().map(|m| m.len() as usize + 1).unwrap_or(0);
	let mut contents = Vec::with_capacity(s);
	file.read_to_end(&mut contents)?;

	let decrypted = decrypt(&contents, path)?;

	Ok(decrypted)
}

fn encrypt(file_data: &[Byte], file_type: CMFileType) -> Result<Vec<Byte>> {
	let inner = Vec::new();
	let mut writer = Cursor::new(inner);
	let file_type_str_buffer = file_type.as_bytes();
	let file_type_str = String::from_utf8(file_type_str_buffer.to_vec())?;
	let file_type_str = file_type_str.trim_end_matches('\0');
	let mut encryption_key = crc16::get_crc(file_type_str.as_bytes(), None, None);
	encryption_key = crc16::get_crc(file_type_str_buffer, None, Some(encryption_key));

	let mut encrypted_data = file_data.to_vec();
	let data_crc = base_encrypt(&mut encrypted_data, &mut encryption_key);

	// Offset 0 (file signature)
	writer.write_u32::<LittleEndian>(FILE_SIGNATURE)?;

	// Offset 4 (header crc - to be built after the header is fully formed)
	writer.write_i32::<LittleEndian>(0)?;

	// Offset 8 (timestamp)
	writer.write_i32::<LittleEndian>(Utc::now().timestamp() as i32)?;

	// Offset 12 (data length)
	writer.write_u32::<LittleEndian>(file_data.len() as u32)?;

	// Offset 16 (data crc)
	writer.write_u32::<LittleEndian>(u32::from(data_crc))?;

	// Offset 20 (file type)
	writer.write_i32::<LittleEndian>(file_type as i32)?;

	// Offset 24-32 (file type string)
	debug_assert_eq!(file_type_str_buffer.len(), 8);
	writer.write_all(file_type_str_buffer)?;

	// Body
	writer.write_all(&encrypted_data)?;

	// Get the header bytes, calculate the crc, and insert the crc into the header
	writer.set_position(0);
	let mut header = Vec::with_capacity(32);
	writer.read_exact(&mut header)?;
	debug_assert_eq!(header.len(), 32);
	let header_crc = crc16::get_crc(&header, None, None);
	writer.set_position(4);
	// We can do this because we are using LittleEndian
	writer.write_u32::<LittleEndian>(u32::from(header_crc))?;

	let mut end = Vec::new();
	writer.set_position(0);
	writer.read_to_end(&mut end)?;
	Ok(end)
}

fn decrypt(buffer: &[Byte], file: PathBuf) -> Result<Vec<Byte>> {
	let mut reader = Cursor::new(buffer);

	if reader.read_u32::<LittleEndian>()? != FILE_SIGNATURE {
		// debug!("Bad file signature, {:?}", file);
		bail!(Error::BadFileSignature(file));
	}

	// Header crc is at offset 4, written as 4 bytes (but still a crc16)
	// (this is a crc of the first 32 bytes (where the crc bytes are 0)
	reader.set_position(4);
	let header_crc = reader.read_u16::<LittleEndian>()?;

	// Timestamp is at offset 8, written as 4 bytes
	reader.set_position(8);
	let _timestamp = reader.read_i32::<LittleEndian>()?;

	// Length is at offset 12, written as 4 bytes
	reader.set_position(12);
	let data_length = reader.read_u32::<LittleEndian>()?;

	// Data crc is at offset 16, written as 4 bytes (but still a crc16)
	reader.set_position(16);
	let data_crc = reader.read_u16::<LittleEndian>()?;

	// File type is at offset 20, written as 4 bytes
	reader.set_position(20);
	let _file_type = reader.read_i32::<LittleEndian>()?;

	// File type (string) is at offset 24, written as 8 bytes, padded with 00
	reader.set_position(24);
	let mut file_type_str_buffer = Vec::with_capacity(8);
	reader.read_exact(&mut file_type_str_buffer)?;

	reader.set_position(24);
	let int_file_type = reader.read_u32::<LittleEndian>()?;

	if let Some(v) = UNKNOWN_MAPS.get(&int_file_type) {
		file_type_str_buffer = v.to_le_bytes().to_vec();
	}

	let mut blob = Vec::new();
	for byte in &file_type_str_buffer {
		if *byte == 0 {
			break;
		};
		blob.push(*byte);
	}

	// First CRC the file type name, then get CRC the file type name (including zeroed bytes)
	let mut encryption_key = crc16::get_crc(&blob, None, None);
	encryption_key = crc16::get_crc(&file_type_str_buffer, None, Some(encryption_key));

	// Data is at offset 32
	reader.set_position(32);
	let mut data = Vec::with_capacity(data_length as usize);
	reader.read_exact(&mut data)?;
	let calculated_data_crc = base_decrypt(&mut data, &mut encryption_key);

	if data_crc != calculated_data_crc {
		debug!("File has an invalid data crc, {:?}, not a fatal error, continuing!", file);
	}

	// WARN: Look at this in detail
	if reader.position() != buffer.len() as u64 {
		debug!("File has trailing bytes, {:?} {:?}, not a fatal error, continuing!", &buffer[reader.position() as usize..], file);
	}

	reader.set_position(0);
	let mut header = Vec::with_capacity(32);
	reader.read_exact(&mut header)?;
	header[4..=7].copy_from_slice(&[0_u8; 4]);

	let calculated_header_crc = crc16::get_crc(&header, None, None);
	if header_crc != calculated_header_crc {
		debug!("File has an invalid header crc, {:?}", file);
	}

	Ok(data)
}

fn base_encrypt(buffer: &mut [Byte], key: &mut u16) -> u16 {
	let mut data_crc: u16 = 0xFFFF;
	for byte in buffer {
		let key_bytes = key.to_le_bytes();
		*key = crc16::get_crc_byte(*byte ^ key_bytes[1], None) ^ u16::from(key_bytes[0]);
		let data_crc_bytes = data_crc.to_le_bytes();
		data_crc = crc16::get_crc_byte(*byte ^ data_crc_bytes[1], None) ^ u16::from(data_crc_bytes[0]);
		*byte ^= key_bytes[0];
	}

	data_crc
}

fn base_decrypt(buffer: &mut [Byte], key: &mut u16) -> u16 {
	let mut data_crc: u16 = 0xFFFF;
	for byte in buffer {
		let key_bytes = key.to_le_bytes();
		*byte ^= key_bytes[0];
		*key = crc16::get_crc_byte(*byte ^ key_bytes[1], None) ^ u16::from(key_bytes[0]);
		let data_crc_bytes = data_crc.to_le_bytes();
		data_crc = crc16::get_crc_byte(*byte ^ data_crc_bytes[1], None) ^ u16::from(data_crc_bytes[0]);
	}

	data_crc
} // NOTE: Isn't this same as the functon in crc16.rs? It probably isn't but it felt very similiar on first view

#[derive(Serialize, Deserialize)]
struct LeItem {
	#[serde(rename = "GUID")]
	guid: String,
	#[serde(rename = "Name")]
	name: String,
}

pub fn dump_lightning(path: PathBuf, dumb_path: Option<PathBuf>) -> Result<()> {
	// Manually add untranslated effect names
	let mut names_by_guid: HashMap<String, String> = hashmap![
		"28E53269-73CC-48c0-B437-C74837B8CD0E".to_string() => "Music volume light effect 2".to_string(),
		"B3370967-FE81-4733-A54C-1FF3D955E942".to_string() => "fn1 lower lamp position change cherry 1".to_string(),
		"B70DC715-98F5-40f1-A3A3-86A7E6C95984".to_string() => "Full bright yellow light".to_string(),
		"CA48BB92-593B-4891-A52F-41E8FB04BF8B".to_string() => "Synchronous RGB gradient".to_string(),
		"DA3AF708-4B88-4ae8-B92E-DD1221A563CF".to_string() => "Music volume lighting effect".to_string(),
		"E25817F8-DFF9-4cc5-A393-DC0EF3D4E646".to_string() => "CSGO game lighting effects".to_string(),
	]; // WARN: Yikes....

	if !path.exists() {
		DirBuilder::new().recursive(true).create(&path)?;
	} else if !path.is_dir() {
		bail!(format!("At the path is not located directory but something else: {:?}", path))
	}

	let dumb_path = dumb_path.unwrap_or_else(|| path.join("dump"));

	if !dumb_path.exists() {
		DirBuilder::new().recursive(true).create(&dumb_path)?;
	} else if !dumb_path.is_dir() {
		bail!(format!("At the path is not located directory but something else: {:?}", dumb_path))
	}

	let le_list: Vec<LeItem> = {
		let contents = fs::read_to_string(path.join("lelist_en").with_extension("json"))?;
		serde_json::from_str(&contents)?
	};

	for item in le_list {
		names_by_guid.insert(item.guid, item.name);
	}

	for path in std::fs::read_dir(path)?
		.filter_map(Result::ok)
		.map(|s| s.path())
		.filter(|f: &PathBuf| f.extension() == Some(OsStr::new("le")))
	{
		let json: Value = {
			let contents = fs::read_to_string(&path)?;
			serde_json::from_str(&contents)?
		};

		let guid = match path.file_name() {
			Some(v) => v.to_str().unwrap_or("Missing GUID!").to_string(), // WARN: YIKES... do something else about this or you will get slap on butt, butt this shouldn't trigger theoretically
			None => bail!("Failed to get file name!?"),
		};

		if let Some(le_name) = names_by_guid.get(&guid) {
			let mut c_file = OpenOptions::new().write(true).create(true).open(dumb_path.join(le_name).with_extension("le"))?;
			c_file.write_all(serde_json::to_string_pretty(&json)?.as_bytes())?;
		} else {
			debug!("Failed to get name for GUID: {}!", guid);
		}
	}

	Ok(())
}
