use smallvec::SmallVec;

pub type Bit = bool;
pub type Byte = u8;

pub type Bits = SmallVec<[Bit; 8 * 64]>;
pub type Bytes = SmallVec<[Byte; 64]>;

pub const ZERO: bool = false;
pub const ONE: bool = true;

pub fn bytes_to_bits(bytes: &[Byte]) -> Bits {
	let mut result = SmallVec::with_capacity(bytes.len() * 8);
	for (i, v) in result.iter_mut().enumerate() {
		*v = (bytes[i / 8] & (1 << (i % 8))) != 0;
	}

	result
}

pub fn bits_to_bytes(bits: &[Bit]) -> Bytes {
	let mut result = SmallVec::with_capacity(bits.len() / 8);
	for (i, v) in bits.iter().enumerate() {
		if v == &ONE {
			result[i / 8] |= 1 << (i % 8);
		}
	}

	result
}
