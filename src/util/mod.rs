use std::{path::PathBuf, time::Duration};

use lazy_static::lazy_static;

pub mod bithelper;
pub mod cm_file;
pub mod config;
pub mod crc16;
pub mod localization;
pub mod packet;
pub mod user_data_file;
pub mod version;

pub const DUR: Duration = Duration::new(0, 150_000_000);
pub const TIMEOUT: Duration = Duration::from_millis(250); // 0.25 secs

lazy_static! {
	pub static ref BASE_PATH: PathBuf = std::env::current_exe().unwrap().parent().unwrap().to_owned();
	pub static ref DATA_BASE_PATH: PathBuf = BASE_PATH.join("data");
	pub static ref USER_DATA_PATH: PathBuf = BASE_PATH.join("userdata");
	pub static ref MODEL_LIST_PATH: PathBuf = DATA_BASE_PATH.join("device").join("modellist").with_extension("json");
	pub static ref LIGHTNING_FOLDER_PATH: PathBuf = DATA_BASE_PATH.join("lighting");
	pub static ref LANG_FOLDER_PATH: PathBuf = DATA_BASE_PATH.join("i18n").join("langs");
	pub static ref CONFIG_PATH: PathBuf = USER_DATA_PATH.join("settings").with_extension("json");
}
