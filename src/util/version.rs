pub const TYPE: Type = Type::Alpha;
pub const VERSION: &str = env!("CARGO_PKG_VERSION");

#[allow(dead_code)]
/// A way to mark version
pub enum Type {
	Alpha,
	Beta,
	ReleaseCandidate, // aka Gamma, Delta
	StableRelease,
	Experimental,
}

impl std::fmt::Display for Type {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Self::Alpha => write!(f, "A"),
			Self::Beta => write!(f, "B"),
			Self::ReleaseCandidate => write!(f, "RC"),
			Self::StableRelease => write!(f, "R"),
			Self::Experimental => write!(f, "EXP"),
		}
	}
}
