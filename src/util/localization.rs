use std::{
	collections::{hash_map::Entry, HashMap},
	fmt::Display,
	fs,
	path::PathBuf,
	sync::Mutex,
};

use anyhow::{bail, Result};
use lazy_static::lazy_static;
use serde::{ser::SerializeStruct, Deserialize, Deserializer, Serialize, Serializer};

use crate::{uni::Error, util::LANG_FOLDER_PATH};

lazy_static! {
	static ref CURRENT_LANGUAGE: Mutex<Language> = Mutex::new(Language::English);
	static ref TRANSLATED_STRINGS: Mutex<HashMap<Language, HashMap<&'static str, &'static str>>> = Mutex::new(load_default_language().expect("Failed to load default language!"));
}

pub fn change_language(lang: Language) -> Result<()> {
	let mut unlocked = match TRANSLATED_STRINGS.try_lock() {
		Ok(v) => v,
		Err(_) => bail!(Error::CurrentLangIsPoisoned),
	};

	let mut curr_lang = match CURRENT_LANGUAGE.try_lock() {
		Ok(v) => v,
		Err(_) => bail!(Error::CurrentLangIsPoisoned),
	};

	if let Entry::Vacant(e) = unlocked.entry(lang) {
		let language_set: HashMap<String, HashMap<String, String>> = {
			let contents = fs::read_to_string(lang.path())?;
			serde_json::from_str(&contents)?
		};

		let mut mod_language_set: HashMap<&'static str, &'static str> = HashMap::new();
		for (_, part_set) in language_set {
			for (key, translated_string) in part_set {
				mod_language_set.insert(Box::leak(key.into_boxed_str()), Box::leak(translated_string.into_boxed_str()));
			}
		}
		e.insert(mod_language_set);
	}

	*curr_lang = lang;

	Ok(())
}

fn load_default_language() -> Result<HashMap<Language, HashMap<&'static str, &'static str>>> {
	let lang = Language::English; // FIXME: ??? This should be loaded from somewhere and if failed default english, settings

	let language_set: HashMap<String, HashMap<String, String>> = {
		let contents = fs::read_to_string(lang.path())?;
		serde_json::from_str(&contents)?
	};

	let mut mod_language_set: HashMap<&'static str, &'static str> = HashMap::new();
	for (_, part_set) in language_set {
		for (key, translated_string) in part_set {
			mod_language_set.insert(Box::leak(key.into_boxed_str()), Box::leak(translated_string.into_boxed_str()));
		}
	}

	let mut final_language_set = HashMap::new();
	final_language_set.insert(lang, mod_language_set);
	Ok(final_language_set)
}

// fn load_language(lang: Language, unlocked: &mut MutexGuard<HashMap<Language, HashMap<&'static str, &'static str>>>) -> Result<()> {
//     if !unlocked.contains_key(&lang) {
//         let file = OpenOptions::new().read(true).open(lang.path())?;

//         let language_set: HashMap<String, String> = serde_json::from_reader(file)?;

//         let mut mod_language_set: HashMap<&'static str, &'static str> = HashMap::new();
//         for (key, value) in language_set {
//             mod_language_set.insert(Box::leak(key.into_boxed_str()), Box::leak(value.into_boxed_str()));
//         }

//         unlocked.insert(lang, mod_language_set);
//     }
//     Ok(())
// }

#[repr(u8)]
#[derive(Eq, PartialEq, Hash, Debug, strum_macros::Display, Clone, Copy, Serialize, Deserialize)]
pub enum Language {
	Czech,   // cz
	English, // en
	Chinese, // zh
}

impl Language {
	fn path(self) -> PathBuf {
		let lang_str = match self {
			Self::Chinese => "zh",
			Self::English => "en",
			Self::Czech => "cz",
		};
		LANG_FOLDER_PATH.join(lang_str).with_extension("json")
	}
}

#[derive(Debug, Clone)]
pub struct LString {
	key: String, // Maybe make enumn for all possible strings?
	// #[serde(skip)]
	cache: &'static str,
}

impl Serialize for LString {
	fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
		// 1 is the number of fields in the struct.
		let mut state = serializer.serialize_struct("LString", 1)?;
		state.serialize_field("key", &self.key)?;
		state.end()
	}
}

impl<'de> Deserialize<'de> for LString {
	fn deserialize<D: Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
		let s: String = Deserialize::deserialize(deserializer)?;
		match Self::new(s) {
			Ok(v) => Ok(v),
			Err(_) => Err(serde::de::Error::custom("Failed to parse LString!")),
		}
	}
}

impl LString {
	pub fn update(&mut self) -> Result<()> {
		let curr_lang = match CURRENT_LANGUAGE.try_lock() {
			Ok(v) => v,
			Err(_) => bail!(Error::CurrentLangIsPoisoned),
		};

		let unlocked = match TRANSLATED_STRINGS.try_lock() {
			Ok(v) => v,
			Err(_) => bail!(Error::CurrentLangIsPoisoned),
		};

		let lang_set = match unlocked.get(&curr_lang) {
			Some(v) => v,
			None => bail!(Error::LanguageNotFound(*curr_lang)),
		};

		self.cache = lang_set.get(&*self.key).copied().unwrap_or("#ERR");

		// self.cache = match lang_set.get(&*self.key).copied() {
		// 	Some(v) => v,
		// 	None => "#ERR", // bail!(Error::BadKeyForLString)
		// };

		Ok(())
	}

	pub fn new(key: String) -> Result<Self> {
		let curr_lang = match CURRENT_LANGUAGE.try_lock() {
			Ok(v) => v,
			Err(_) => bail!(Error::CurrentLangIsPoisoned),
		};

		let unlocked = match TRANSLATED_STRINGS.try_lock() {
			Ok(v) => v,
			Err(_) => bail!(Error::CurrentLangIsPoisoned),
		};

		let lang_set = match unlocked.get(&curr_lang) {
			Some(v) => v,
			None => bail!(Error::LanguageNotFound(*curr_lang)),
		};

		// FIXME: FIX THIS
		let cache = lang_set.get(&*key).copied().unwrap_or("#ERR");
		// copied() here clones the reference not the string!
		/*bail!(Error::BadKeyForLString)*/ // Couldn't find a translated string

		Ok(Self { key, cache })
	}
}

impl Display for LString {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.cache)
	}
}
