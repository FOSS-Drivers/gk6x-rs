use std::{
	io::{Cursor, Seek, SeekFrom, Write},
	ops::{Deref, DerefMut},
};

use super::bithelper::Byte;

pub const LENGTH_PACKET_DATA: usize = 0x38; // LENGTH_PACKET_DATA == 56

type MemoryStream /*AHAHAHAHAH*/ = Cursor<Vec<u8>>;

pub struct Packet(MemoryStream);

impl Default for Packet {
	fn default() -> Self {
		Self(MemoryStream::new(Vec::with_capacity(LENGTH_PACKET_DATA)))
	}
}

impl Seek for Packet {
	fn seek(&mut self, pos: SeekFrom) -> std::io::Result<u64> {
		self.0.seek(pos)
	}
}

impl Write for Packet {
	fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
		self.0.write(buf)
	}

	fn flush(&mut self) -> std::io::Result<()> {
		self.0.flush()
	}
}

impl Deref for Packet {
	type Target = MemoryStream;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for Packet {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl Packet {
	#[inline]
	pub fn get_buffer(&mut self) -> &mut [Byte] {
		self.get_mut()
	}
}
