use std::{
	collections::{HashMap, HashSet},
	default::default,
};

use anyhow::Result;
// use num::FromPrimitive;
// use serde::{Deserialize, Deserializer};
// use tracing::debug;

use crate::{
	keyboard::state::KeyJS,
	mapping::{
		enums::{KeyboardLayer, LightingEffectColorType, LightingEffectType, MacroKeyState, MacroKeyType, MacroRepeatType},
		key_values::{
			self,
			DriverValue::{self, UnusedKeyValue},
			DriverValueModifier, DriverValueType,
		},
	},
};

use super::bithelper::Byte;

struct ARGB {
	a: Byte,
	r: Byte,
	g: Byte,
	b: Byte,
}

struct UserDataFile {
	key_aliases: HashMap<String, String>,
	pub fn_layers: HashMap<KeyboardLayer, Layer>,
	pub layers: HashMap<KeyboardLayer, Layer>,
	pub lightning_effects: HashMap<String, LightingEffect>,
	pub macros: HashMap<String, Macro>,
	next_lightning_id: i32,
	next_macro_id: i32,
	/// If true all lighting data on the keyboard will be cleared (this is different to not providing any lighting which will just skip sending lighting data)
	pub no_lightning: bool,
	pub no_lightning_layers: HashSet<KeyboardLayer>,
}

impl UserDataFile {
	pub fn save(&mut self) -> Result<()> {
		let mut builder = String::new();
		if !self.macros.is_empty() {
			for mac in self.macros.values() {
				builder.push('\n');
				builder.push_str(&format!(
					"[Macro({name},{delay},{r_type},{count})]",
					name = mac.name,
					delay = mac.default_delay,
					r_type = mac.repeat_type,
					count = mac.repeat_count
				));
				for action in &mac.actions {
					let delay = if action.delay > 0 { format!(":{}", action.delay) } else { String::new() };
					let modifier = match &action.modifier {
						Some(modifier) => format!("{}", modifier),
						None => String::new(),
					};
					let key = format!(
						"{}{}",
						if !modifier.is_empty() { "+" } else { "" },
						if action.key_code > 0 {
							format!("{}", key_values::SHORT_TO_LONG_DRIVER_VALUES.get(&action.key_code).unwrap_or(&DriverValue::UnusedKeyValue))
						} else {
							String::new()
						}
					);
					// string keyStr = (!string.IsNullOrEmpty(modifierStr) ? "+" : string.Empty) + (action.KeyCode > 0 ? ((DriverValue) KeyValues.GetLongDriverValue(action.KeyCode)).ToString() : string.Empty);
					if !modifier.is_empty() || !key.is_empty() {
						builder.push('\n');
						builder.push_str(&format!("{}:{}{}{}", action.type_t, modifier, key, delay));
					}
					// if (!string.IsNullOrEmpty(modifierStr) || !string.IsNullOrEmpty(keyStr))
					// 	sb.AppendLine(action.Type + ":" + modifierStr + keyStr + delayStr);
				}
				builder.push('\n');
			}
			builder.push('\n');
		}

		if !self.lightning_effects.is_empty() {
			// TODO: Copy lighting effect file over from data file
			// Dictionary<string, string> names = LightingEffect.Discover();
			// foreach (KeyValuePair<string, LightingEffect> le in LightingEffects)
			// {
			// 	string name;
			// 	if (le.Value.Layers.Count > 0 && names.TryGetValue(le.Key.ToLower(), out name))
			// 		sb.AppendLine("Lighting[(" + name + ")," + string.Join(",", le.Value.Layers) + "]");
			// }

			builder.push('\n');
		}

		// 	for (int i = 0; i < 2; i++)
		// 	foreach (KeyValuePair<KeyboardLayer, Layer> layer in i == 0 ? Layers : FnLayers)
		// 	{
		// 		sb.AppendLine("[" + (i > 0 ? "Fn" : string.Empty) + layer.Key + "]");
		// 		uint[] defaultDriverValues = i > 0 ? keyboard.GetLayer(layer.Key).FnKeySet : keyboard.GetLayer(layer.Key).KeySet;
		// 		for (int j = 0; j < keyboard.MaxLogicCode; j++)
		// 		{
		// 			KeyboardState.Key key = keyboard.GetKeyByLogicCode(j);
		// 			if (key != null && j < defaultDriverValues.Length)
		// 			{
		// 				uint driverValue = layer.Value.GetKey(key);
		// 				if (driverValue != KeyValues.UnusedKeyValue && driverValue != defaultDriverValues[j])
		// 					switch (KeyValues.GetKeyType(driverValue))
		// 					{
		// 						case DriverValueType.Macro:
		// 						{
		// 							byte macroId = KeyValues.GetKeyData2(driverValue);
		// 							Macro macro = Macros.Values.FirstOrDefault(x => x.Id == macroId);
		// 							if (macro != null)
		// 								sb.AppendLine((DriverValue) defaultDriverValues[j] + ":Macro(" +
		// 											  macro.Name + ")");
		// 						}
		// 							break;
		// 						default:
		// 							sb.AppendLine((DriverValue) defaultDriverValues[j] + ":" +
		// 										  (DriverValue) driverValue);
		// 							break;
		// 					}
		// 			}
		// 		}

		// 		sb.AppendLine();
		// 	}

		// File.WriteAllText(path, sb.ToString());

		Ok(())
	}

	pub fn get_number_of_macros(&self) -> usize {
		let mut macro_ids = HashSet::new();
		for layer in self.layers.values().chain(self.fn_layers.values()) {
			for dv in layer.keys.values() {
				if key_values::get_key_type(*dv) == Some(DriverValueType::Macro) {
					macro_ids.insert(key_values::get_key_data_for_drivervaluemodifer(*dv));
				}
			}
		}
		macro_ids.len()
	}

	#[inline]
	pub fn add_or_find_layer(&mut self, layer: KeyboardLayer, fn_key: bool) -> &mut Layer {
		let layers = if fn_key { &mut self.fn_layers } else { &mut self.layers };

		layers.entry(layer).or_insert_with(Layer::default)
	}

	#[inline]
	pub fn get_macro(&self, name: &str) -> Option<&Macro> {
		self.macros.get(name)
	}

	#[inline]
	pub fn get_lightning_effect(&self, name: &str) -> Option<&LightingEffect> {
		self.lightning_effects.get(name)
	}

	#[inline]
	pub fn get_lightning_effects(&self, layer: KeyboardLayer) -> Vec<&LightingEffect> {
		self.lightning_effects.values().filter(|s| s.layers.contains(&layer)).collect()
	}
}

#[derive(Default)]
struct Macro {
	pub actions: Vec<Action>,
	pub default_delay: u16,
	/// Used only for .cmf macros
	pub guid: String,
	pub id: i32,

	/// Key names as defined in the "Macros" tab (this is seemingly not in the data files?)
	key_names: HashMap<String, DriverValue>,

	pub name: String,
	pub repeat_count: u8,
	pub repeat_type: MacroRepeatType,

	/// If true a delay will be used on the last action. This can be useful where
	/// the macro is to be repeated multiple times and a constant delay is desired.
	pub use_trailing_delay: bool,
}

impl Macro {
	pub fn new(name: String) -> Self {
		Self {
			name,
			id: -1,
			//repeat_type: MacroRepeatType::RepeatXTimes,
			repeat_count: 1,
			..default() // NOTE: Unstable feature, remove if stack overflows
		}
	}
}

struct Action {
	pub delay: u16,
	/// DriverValueMouseButton or short version of `DriverValue`
	pub key_code: Byte,
	// #[serde(deserialize_with = "hex_numstring_to_drivervaluemodifier")]
	pub modifier: Option<DriverValueModifier>,
	pub state: MacroKeyState,
	pub type_t: MacroKeyType,

	/// Used for the web gui string name mappings `MacroKeyNames`
	pub value_string: String,
}

// fn hex_numstring_to_drivervaluemodifier<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Option<DriverValueModifier>, D::Error> {
// 	let s: String = Deserialize::deserialize(deserializer)?;

// 	// Be careful! The hex number sometimes contains ending whitespace!! Traitor!!
// 	return Ok(FromPrimitive::from_u32(match u32::from_str_radix(s.trim_start_matches("0x").trim_end(), 16){
// 		Ok(v) => v,
// 		Err(_) => {
// 			debug!("Failed to convert \"{}\" to number!", s);
// 			return Err(serde::de::Error::custom("Failed to parse Hex Number String to Number!"));
// 		}
// 	}));
// }

struct LightingEffect {
	// owner: UserDataFile,
	/// Each frame holds an array of keys (location codes)
	pub frames: Vec<Frame>,
	pub id: i32,
	/// Used for static lighting. This maps from key location codes to an RGB value
	pub key_colors: HashMap<i32, u32>,
	/// The layers this effect should be used on
	pub layers: HashSet<KeyboardLayer>,
	pub name: String,
	/// Lighting effect params / configs
	pub params: Vec<Param>,
	/// Total number of frames (based on the frame count for each frame)
	pub total_frames: i32,
	pub type_t: LightingEffectType,
}

impl LightingEffect {
	/// Total number of keys as seen by the lighting system
	// (528/0x210 total bytes - see refs to this number in the disassembled code)
	pub const NUM_KEYS: u8 = 132;

	pub const MAX_EFFECTS: u8 = 32;

	/// Total number of bytes used for static lighting (1 uint color value for each key)
	// (704/0x2C0 bytes, 176 ints)
	pub const NUM_STATIC_LIGHTING_BYTES: u16 = 704;
}

struct Frame {
	/// Number of frames this frame should be displayed
	pub count: i32,
	/// Key location codes
	pub key_codes: HashSet<i32>,
}

struct Param {
	pub color: u32,
	pub color_type: LightingEffectColorType,
	pub keys: HashSet<i32>,

	/// If true the values used by RGB/breathing should be sent to the keyboard unmodified (regular lighting effect
	/// files have their values modified by an amount - 360/val for RGB, 100/val for breathing val2)
	pub use_raw_values: bool,

	/// "Count" (used in RGB / breathing)
	pub val1: i32,
	/// "StayCount" (used in breathing)
	pub val2: i32,
}

#[derive(Default)]
struct Layer {
	pub keys: HashMap<String, DriverValue>,
}

impl Layer {
	fn get_key(&self, key: &KeyJS) -> DriverValue {
		let result = self.keys.get(&key.driver_value_name.to_lowercase());

		match result {
			None => UnusedKeyValue,
			Some(v) => *v,
		}
	}
}

enum GroupType {
	Layer,
	Macro,
	Lighting,
	KeyAlias,
}
