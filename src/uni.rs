use std::path::PathBuf;

use thiserror::Error;

use crate::util::localization::Language;

#[repr(u8)]
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
	#[error("Failed to find model \"{0}\"!")]
	FailedToFindModel(u32),
	#[error("Failed to load keys for this state!")]
	FailedToLoadKeys,
	#[error("Couldn't get default driver values!")]
	DefaultProfileDriverValues,
	#[error("The packet the function received wasn't meant for it as response to packet it sent!")]
	CaughtWrongPacket,
	#[error("The keyboard did not respond!")]
	NoResponse,
	#[error("TODO")]
	NotEnoughBytes,
	#[error("Failed to start listening thread for disconnect & connect of HID devices aka Keyboards and such. It's very likely running already!")]
	FailedToStartListeningThread,
	#[error("Failed to stop listening thread for disconnect & connect of HID devices aka Keyboards and such.")]
	FailedToStopListeningThread,
	//ListeningThreadIsAlreadyRunning,
	#[error("Result buffer report ID does not equal 0!")]
	ResultBufferReportID,
	#[error("One of the handshake packets has result code different than 0!")]
	HandshakeResultCode,
	#[error("CRC Validation failed!")]
	CRCValidation,
	#[error("BufferSize A & B are wrong!")]
	BufferSize,
	#[error("The Firmware ID is wrong!")]
	FirmwareID,
	#[error("Bad keyboard Model ID!")]
	ModelID,
	#[error("Bad keyboard Model CRC!")]
	ModelCRC,
	#[error("Couldn't find data for keyboard!")]
	KeebData,
	#[error("Keyboard State not found for the {0} model!")]
	KeyboardStateNotFound(u32),
	#[error("Initializing buffer failed!")]
	InitBuffer,
	#[error("This layer is not suppose to be ever used here >_<")]
	BadLayer,
	#[error("No default factory data found for the {0} model!")]
	NoFactoryDefaultData(u32),
	#[error("Failed to load keyboard models!")]
	FailedToLoadKeyboardModels,
	#[error("Failed to load model \"{0}\" from the disk!")]
	FailedToLoadKeyboardModel(u32),
	#[error("This keyboard [{0:04x},{1:04x}] is not supported!")]
	NotSupported(u16, u16),
	#[error("There is no available endpoint which can be used to communicate with the keyboard!")]
	NoProperEndpointFound,
	#[error("Failed to setup driver key set buffers for some reason! Go debug it!")]
	FailedToSetupDriverKeySetBuffers,
	#[error("Failed to set keys!")]
	FailedToSetKeys,
	#[error("The static Keyboard States cache is poisoned!")]
	KeyboardStatesIsPoisoned,
	#[error("The current language mutex is poisoned!")]
	CurrentLangIsPoisoned,
	#[error("The key for LString doesn't have translation!")]
	BadKeyForLString,
	#[error("The language {0} were not found on disk!")]
	LanguageNotFound(Language),
	#[error("Couldn't parse the &str to MP Enum!")]
	FailedToParseMPFromStr,
	#[error("The path {0} is wrong!")]
	PathIsWrong(PathBuf),
	#[error("The file at \"{0}\" has bad signature!")]
	BadFileSignature(PathBuf),
	#[error("The number of bytes to write is not correct!")]
	NumBytesToWriteNotCorrect,
}
