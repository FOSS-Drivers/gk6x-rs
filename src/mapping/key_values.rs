use std::{
	collections::{hash_map::Entry, HashMap},
	fs,
	path::PathBuf,
};

use anyhow::Result;
use lazy_static::lazy_static;
use num::FromPrimitive;
use num_derive::{FromPrimitive, ToPrimitive};
use serde::{Deserialize, Deserializer, Serialize};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use tracing::debug;

use crate::util::{bithelper::Byte, localization::LString, DATA_BASE_PATH};

lazy_static! {
	static ref MC: MagicContainer = load().expect("Failed to load Magic Container! Fatal Error!");
	pub static ref GROUPS: &'static Vec<Group> = MC.groups;
	pub static ref KEYS: &'static HashMap<DriverValue, &'static Key> = &MC.keys;
	pub static ref KEYS_BY_LOGIC_CODE: &'static HashMap<i32, &'static Key> = &MC.keys_by_logic_code;

	// These map full driver values (4 bytes long) to the individual driver key codes (1 byte long)
	// This is only for actual keys (things like VolumeUp don't appear here)
	pub static ref LONG_TO_SHORT_DRIVER_VALUES: &'static HashMap<DriverValue, Byte> = &MC.long_to_short_driver_values;
	pub static ref SHORT_TO_LONG_DRIVER_VALUES: &'static HashMap<Byte, DriverValue> = &MC.short_to_long_driver_values;
}

pub struct MagicContainer {
	groups: &'static Vec<Group>,
	keys: HashMap<DriverValue, &'static Key>,
	keys_by_logic_code: HashMap<i32, &'static Key>,
	long_to_short_driver_values: HashMap<DriverValue, Byte>,
	short_to_long_driver_values: HashMap<Byte, DriverValue>,
}

fn load() -> Result<MagicContainer> {
	let path: PathBuf = DATA_BASE_PATH.join("keys").with_extension("json");

	// REVIEW: Does it work the way it's suppose to??? the logic may be wrong
	let mut long_to_short_driver_values: HashMap<DriverValue, Byte> = HashMap::with_capacity(120); // Allocate in one go
	let mut short_to_long_driver_values: HashMap<Byte, DriverValue> = HashMap::with_capacity(120);
	for value in DriverValue::iter() {
		if get_key_type(value) == Some(DriverValueType::Key) && get_key_modifier(value) == None {
			let short_value: Byte = get_key_data1(value);
			long_to_short_driver_values.insert(value, short_value);
			short_to_long_driver_values.insert(short_value, value);
		}
	}

	let mut keys: HashMap<DriverValue, &'static Key> = HashMap::new();
	let mut keys_by_logic_code: HashMap<i32, &'static Key> = HashMap::new();
	let groups: &Vec<Group> = Box::leak(Box::new({
		let contents = fs::read_to_string(path)?;
		serde_json::from_str(&contents)?
	}));

	for key_group in groups {
		// This is a bit of a mouthful... (vector->hashmap->vector->hashmap)
		for key in &key_group.keys {
			if key.driver_value != DriverValue::Disabled {
				if let Entry::Vacant(e) = keys.entry(key.driver_value) {
					e.insert(key);
					if let Some(v) = key.logic_code {
						keys_by_logic_code.insert(v, key);
					} else {
						debug!("The key \"{}\" has no logic code!", key.hname());
					}
				} else {
					debug!("Duplicate key \"{}\" - \"{:#0X?}\" (keys.json)", key.hname(), key.driver_value as u32);
				}
			}
		}
	}

	Ok(MagicContainer {
		groups,
		keys,
		keys_by_logic_code,
		long_to_short_driver_values,
		short_to_long_driver_values,
	})
}

/// Used to hold the actual value of the key
#[allow(clippy::cast_possible_truncation)]
const fn get_key_data1(driver_value: DriverValue) -> Byte {
	// (driver_value as u32).to_le_bytes()[1]
	(driver_value as u32 >> 8) as Byte
}

fn is_key_modifier(driver_value: DriverValue) -> bool {
	get_key_type(driver_value) == Some(DriverValueType::Key) && get_key_modifier(driver_value) != None && get_key_data1(driver_value) == 0
}

/// Used to hold the value of the key and modifier keys (as well as other things on other key types)
const fn get_key_data(driver_value: DriverValue) -> u16 {
	driver_value as u16
}

fn get_key_modifier(driver_value: DriverValue) -> Option<DriverValueModifier> {
	FromPrimitive::from_u8(get_key_data_for_drivervaluemodifer(driver_value))
}

/// Used to hold additional data (such as the modifiers / macro index / keyboard layer / mouse button)
pub const fn get_key_data_for_drivervaluemodifer(driver_value: DriverValue) -> Byte {
	driver_value as Byte
}

pub fn get_key_type(driver_value: DriverValue) -> Option<DriverValueType> {
	// FromPrimitive::from_u32((driver_value as u32).to_le_bytes()[3])
	FromPrimitive::from_u32(driver_value as u32 >> 16)
}

fn get_mouse_button(driver_value: DriverValue) -> Option<DriverValueMouseButton> {
	if get_key_type(driver_value) == Some(DriverValueType::Mouse) {
		return FromPrimitive::from_u8(get_key_data_for_drivervaluemodifer(driver_value));
	}
	None
}

// pub fn get_short_driver_value(long_value: DriverValue) -> Option<&'static Byte> {
// 	LONG_TO_SHORT_DRIVER_VALUES.get(&long_value)
// }

// pub fn get_long_driver_value(short_value: Byte) -> Option<&'static DriverValue> {
// 	SHORT_TO_LONG_DRIVER_VALUES.get(&short_value)
// }

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Group {
	#[serde(rename = "keytype")]
	pub key_type: String,
	#[serde(rename = "pname")]
	pub p_name: String,
	pub pid: String,
	// #[serde(borrow)]
	pub title: String,
	pub lang: String,
	#[serde(deserialize_with = "linekeys")]
	pub keys: Vec<Key>,
}

fn linekeys<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Vec<Key>, D::Error> {
	let mut s: Vec<HashMap<String, Vec<Key>>> = Deserialize::deserialize(deserializer)?;

	let mut r = Vec::new();
	for v in &mut s {
		r.append(match v.get_mut("linekeys") {
			Some(v) => v,
			None => return Err(serde::de::Error::custom("Failed to parse linekeys!")),
		});
	}

	Ok(r)
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Key {
	// The owning group
	// #[serde(skip)]
	// pub group: Option<Group>,
	/// Where the key appears visually
	#[serde(rename = "LocationCode")]
	pub location_code: Option<i32>,
	/// Where the index of the key as defined in the keyboard profile.json
	#[serde(rename = "LogicCode")]
	pub logic_code: Option<i32>,
	/// The name of the key
	#[serde(rename = "Name")]
	pub name: String,
	#[serde(rename = "Class")]
	pub class: Option<String>,
	#[serde(rename = "IsCombFunc")]
	pub is_comb_func: Option<bool>,
	/// The localized name of the key (can be null)
	// #[serde(borrow)]
	#[serde(rename = "LangTitle")]
	pub title: Option<LString>,
	/// The key value which the keyboard firmware understands
	#[serde(rename = "DriverValue")]
	#[serde(deserialize_with = "hex_numstring_to_driver_value")]
	pub driver_value: DriverValue,
	// Used for disabling multiple keys
	// #[serde(skip)]
	// pub driver_value_array: Bytes,
}

impl Key {
	pub fn hname(&self) -> String {
		let mut name = match &self.title {
			Some(v) => format!("{}", v),
			None => self.name.clone(),
		};
		if name == "#ERR" {
			name = self.name.clone();
		}
		name
	}
}

fn hex_numstring_to_driver_value<'de, D: Deserializer<'de>>(deserializer: D) -> Result<DriverValue, D::Error> {
	let s: String = Deserialize::deserialize(deserializer)?;

	// Be careful! The hex number sometimes contains ending whitespace!! Traitor!!
	return Ok(match u32::from_str_radix(s.trim_start_matches("0x").trim_end(), 16) {
		Ok(v) => match FromPrimitive::from_u32(v) {
			Some(v) => v,
			None => {
				debug!("Failed to cast \"{:#0X?}\" to DriverValue!", v);
				DriverValue::UnusedKeyValue
			}
		},
		Err(_) => {
			debug!("Failed to convert \"{}\" to number!", s);
			DriverValue::Disabled
			//return Err(serde::de::Error::custom("Failed to parse Hex Number String to Number!"));
		}
	});
}

// Fn key note:
// There isn't any way to reprogram the Fn key. It's technically possible to assign a key to the Fn key but the
// base functionality of the Fn key cannot be overridden. Assigning a key to the Fn key only works on the Fn key set
// as pressing Fn instantly switches to the Fn key set. Assigning a key to the Fn key in this way is completely
// pointless, but it gives some insight into how things work.

// Base layer note:
// The base layer is programmable, but the Fn key on base layer doesn't seem to be (possibly disabled out of fear
// that a user would unknowingly lock themselves out somehow?) This kind of sucks!

impl Default for DriverValue {
	fn default() -> Self {
		Self::UnusedKeyValue
	}
}

/// The key driver values as defined in the files
#[repr(u32)]
#[derive(Debug, strum_macros::Display, Clone, Serialize, Deserialize, PartialEq, Eq, Hash, FromPrimitive, ToPrimitive, Copy, EnumIter)]
pub enum DriverValue {
	/// Unused key valey / invalid key value. Used for keys which aren't mapped on the keyboard.
	UnusedKeyValue = 0xFFFFFFFF,
	// None = 0,

	///////////////////////////
	// Primary
	///////////////////////////
	Esc = 0x02002900,
	/// Disables the key
	Disabled = 0x02000000,
	F1 = 0x02003A00,
	F2 = 0x02003B00,
	F3 = 0x02003C00,
	F4 = 0x02003D00,
	F5 = 0x02003E00,
	F6 = 0x02003F00,
	F7 = 0x02004000,
	F8 = 0x02004100,
	F9 = 0x02004200,
	F10 = 0x02004300,
	F11 = 0x02004400,
	F12 = 0x02004500,
	PrintScreen = 0x02004600, //PS
	ScrollLock = 0x02004700,  //SL
	Pause = 0x02004800,       //PB
	// --- end line ---
	/// '`' key (backtick/grave/tilde)
	BackTick = 0x02003500,
	// D = decimal (base 10)
	D1 = 0x02001E00,
	D2 = 0x02001F00,
	D3 = 0x02002000,
	D4 = 0x02002100,
	D5 = 0x02002200,
	D6 = 0x02002300,
	D7 = 0x02002400,
	D8 = 0x02002500,
	D9 = 0x02002600,
	D0 = 0x02002700,
	Subtract = 0x02002D00,
	Add = 0x02002E00,
	Backspace = 0x02002A00,
	Insert = 0x02004900,
	Home = 0x02004A00,
	PageUp = 0x02004B00,
	// --- end line ---
	Tab = 0x02002B00,
	Q = 0x02001400,
	W = 0x02001A00,
	E = 0x02000800,
	R = 0x02001500,
	T = 0x02001700,
	Y = 0x02001C00,
	U = 0x02001800,
	I = 0x02000C00,
	O = 0x02001200,
	P = 0x02001300,
	OpenSquareBrace = 0x02002F00,
	CloseSquareBrace = 0x02003000,
	Backslash = 0x02003100, // also 0x02003200
	Delete = 0x02004C00,
	End = 0x02004D00,
	PageDown = 0x02004E00,
	// --- end line ---
	CapsLock = 0x02003900,
	A = 0x02000400,
	S = 0x02001600,
	D = 0x02000700,
	F = 0x02000900,
	G = 0x02000A00,
	H = 0x02000B00,
	J = 0x02000D00,
	K = 0x02000E00,
	L = 0x02000F00,
	Semicolon = 0x02003300,
	Quotes = 0x02003400,
	Enter = 0x02002800,
	// --- end line ---
	LShift0 = 0x02000002,
	LShift1 = 0x2001E02,
	LShift2 = 0x2001F02,
	LShift3 = 0x2002002,
	LShift4 = 0x2002102,
	LShift5 = 0x2002202,
	LShift6 = 0x2002302,
	/// Key between left shift and Z
	AltBackslash = 0x02006400,
	Z = 0x02001D00,
	X = 0x02001B00,
	C = 0x02000600,
	V = 0x02001900,
	B = 0x02000500,
	N = 0x02001100,
	M = 0x02001000,
	Comma = 0x02003600,
	Period = 0x02003700,
	/// '/' and '?'
	Slash = 0x02003800,
	RShift = 0x02000020,
	RShiftApostrophe = 0x2003420,
	RShiftComma = 0x2003620,
	RShiftDot = 0x2003720,
	RShiftSlash = 0x2003820,
	RShiftSemicolon = 0x2003320,
	RShiftLBracket = 0x2002F20,
	RShiftRBracket = 0x2003020,
	RShiftBackslash = 0x2003120,
	Up = 0x02005200,
	LCtrl = 0x02000001,
	LWin = 0x02000008,
	LAlt = 0x02000004,
	Space = 0x02002C00,
	RAlt = 0x02000040,
	RWin = 0x02000080,
	Menu = 0x02006500,
	RCtrl = 0x02000010,
	Left = 0x02005000,
	Down = 0x02005100,
	Right = 0x02004F00,

	///////////////////////////
	// Numpad
	///////////////////////////
	NumLock = 0x02005300,
	NumPadSlash = 0x02005400,
	NumPadAsterisk = 0x02005500,
	NumPadSubtract = 0x02005600,
	// --- end line ---
	NumPad7 = 0x02005F00, // home
	NumPad8 = 0x02006000, // up
	NumPad9 = 0x02006100, // pageup
	NumPadAdd = 0x02005700,
	// --- end line ---
	NumPad4 = 0x02005C00, // left
	NumPad5 = 0x02005D00,
	NumPad6 = 0x02005E00, // right
	// --- end line ---
	NumPad1 = 0x02005900, // end
	NumPad2 = 0x02005A00, // down
	NumPad3 = 0x02005B00, // pagedown
	// --- end line ---
	NumPad0 = 0x02006200,
	NumPadPeriod = 0x02006300, // del
	NumPadEnter = 0x02005800,

	///////////////////////////
	// Media
	///////////////////////////
	OpenMediaPlayer = 0x03000183,
	MediaPlayPause = 0x030000CD,
	MediaStop = 0x030000B7,
	// --- end line ---
	MediaPrevious = 0x030000B6,
	MediaNext = 0x030000B5,
	// --- end line ---
	VolumeUp = 0x030000E9,
	VolumeDown = 0x030000EA,
	VolumeMute = 0x030000E2,

	///////////////////////////
	// System
	///////////////////////////
	BrowserSearch = 0x03000221,
	BrowserStop = 0x03000226,
	BrowserBack = 0x03000224,
	BrowserForward = 0x03000225,
	BrowserRefresh = 0x03000227,
	BrowserFavorites = 0x0300022A,
	BrowserHome = 0x03000223,
	// --- end line ---
	OpenEmail = 0x0300018A,
	OpenMyComputer = 0x03000194,
	OpenCalculator = 0x03000192,
	// --- end line ---
	Copy = 0x02000601,
	Paste = 0x02001901,

	///////////////////////////
	// Mouse
	///////////////////////////
	MouseLClick = 0x01010001,
	MouseRClick = 0x01010002,
	MouseMClick = 0x01010004,
	MouseBack = 0x01010008,
	MouseAdvance = 0x01010010,

	///////////////////////////
	// Keyboard Layers (temporary switch)
	///////////////////////////
	TempSwitchLayerBase = 0x0a070001, // std / standard
	TempSwitchLayer1 = 0x0a070002,
	TempSwitchLayer2 = 0x0a070003,
	TempSwitchLayer3 = 0x0a070004,
	TempSwitchLayerDriver = 0x0a070005, // TODO: Verify this exists

	///////////////////////////
	// The following values aren't defined in any files in the GK6X software
	///////////////////////////

	// https://www.w3.org/TR/uievents-code/
	Power = 0x02006600, // keycode 255 (w3:"Power")
	Clear = 0x02006700, // The CLEAR key
	F13 = 0x02006800,
	F14 = 0x02006900,
	F15 = 0x02006A00,
	F16 = 0x02006B00,
	F17 = 0x02006C00,
	F18 = 0x02006D00,
	F19 = 0x02006E00,
	F20 = 0x02006F00,
	F21 = 0x02007000,
	F22 = 0x02007100,
	F23 = 0x02007200,
	F24 = 0x02007300,
	NumPadComma = 0x02008500, // keycode 194 (w3:"NumpadComma")
	IntlRo = 0x02008700,      // keycode 193 (w3:"IntlRo")
	KanaMode = 0x02008800,    // keycode 255 (w3:"KanaMode")
	IntlYen = 0x02008900,     // keycode 255 (w3:"IntlYen")
	Convert = 0x02008A00,     // keycode 255 (w3:"Convert")
	NonConvert = 0x02008B00,  // keycode 235 (w3:"NonConvert")
	//0x02008C00,// keycode 234 - not sure what this is
	Lang3 = 0x02009200, // keycode 255 (w3:"Lang3")
	Lang4 = 0x02009300, // keycode 255 (w3:"Lang4")
	//F24 = 0x02009400,// keycode 135 (w3:"F24") (duplicate)

	//0x0A020001 - ?
	ToggleLockWindowsKey = 0x0A020002, // Toggles a lock on the windows key
	ToggleBluetooth = 0x0A020007,
	//0x0A020006 - ?
	ToggleBluetoothNoLED = 0x0A020008, // Toggles bluetooth (and disables the bluetooth LED until manually toggled)

	// These are the same as pressing the layer buttons (pressing the button whilst it's active takes you to the base layer)
	// If you want to temporarily switch you should use the TempSwitchXXXX versions instead
	DriverLayerButton = 0x0A060001,
	Layer1Button = 0x0A060002,
	Layer2Button = 0x0A060003,
	Layer3Button = 0x0A060004,

	// 0x0A0700XX seem to do weird things with the lighting (resetting current lighting effect, disabling lighting for
	// as long as you hold down a key) - but these also seem to soft lock the keyboard shortly after, until you replug it
	// RESET button?
	NextLightingEffect = 0x09010010,         // NOTE: Only works on base layer
	NextReactiveLightingEffect = 0x09010011, // NOTE: Only works on base layer
	BrightnessUp = 0x09020001,
	BrightnessDown = 0x09020002,
	LightingSpeedDecrease = 0x09030002,
	LightingSpeedIncrease = 0x09030001,
	LightingPauseResume = 0x09060001,
	ToggleLighting = 0x09060002,
	// These values were found in the GK64 firmware, but don't seem to do anything?
	//0x09010002 - ?
	//0x09011307 - ?
	//0x09010006 - ?
	//0x09010008 - ?
	//0x09010009 - ?
	//0x09010004 - ?
	//0x09010003 - ?
	//0x09010005 - ?
	//0x0901000A - ?

	// Use 0xFEXXXXXX for fake values
	All = 0xFE000001, // Used to assign all keys to a given value

	// TODO: Find Bluetooth buttons 1-3
	// TODO: Find Fn value (if it even exists)
	// TODO: Find flash memory value (if it even exists)
}

#[derive(FromPrimitive, PartialEq, Eq)]
pub enum DriverValueType {
	None = 0,
	Mouse = 0x0101,
	Key = 0x0200,
	/// Open "My Computer", calculator, etc
	System = 0x0300,
	Macro = 0x0A01,
	TempSwitchLayer = 0x0A07,
}

#[derive(FromPrimitive, PartialEq, Eq, Debug, strum_macros::Display)]
pub enum DriverValueModifier {
	LCtrl = 0x01,
	LShift = 0x02,
	LAlt = 0x04,
	LWin = 0x08,
	RCtrl = 0x10,
	RShift = 0x20,
	RAlt = 0x40,
	RWin = 0x80,
}

#[derive(FromPrimitive, PartialEq, Eq)]

pub enum DriverValueMouseButton {
	LButton = 0x01,
	RButton = 0x02,
	MButton = 0x04,
	Back = 0x08,
	Advance = 0x10,
}
