// TODO: When there is better lighting / macro support, put these in the appropriate files

#[repr(u8)]
pub enum LightingEffectType {
	/// Static "DIY" lighting with an RGB value for each key
	Static = 0,

	// NOTE: 2 frames?
	/// Lighting effect with 1 or more frames of lighting
	Dynamic = 3,
}

#[repr(u8)]
pub enum LightingEffectColorType {
	/// Single solid color
	Monochrome = 0,

	/// Color which changes through the color spectrum
	RGB = 1,

	/// Color which changes through the color spectrum, and visually "breathes"
	Breathing = 2,
}

impl Default for MacroRepeatType {
	fn default() -> Self {
		Self::RepeatXTimes
	}
}

/// Defines how a macro should be repeated when pressing a key bound to a macro
#[repr(u8)]
#[derive(Debug, strum_macros::Display)]
pub enum MacroRepeatType {
	/// Repeat the macro X number of times after the key is pressed (subsequent key presses are ignored until the macro has completely finished - there doesn't appear to be any way to stop the macro once it has started, and the key must be released and pressed again to start the macro again after it has finished)
	RepeatXTimes = 1,

	/// Release key to stop the macro (repeats the macro until the key is released (even if the macro is partially complete))
	ReleaseKeyToStop = 2,

	/// Press key a second time to stop the macro (repeats the macro until the key is pressed again)
	PressKeyAgainToStop = 3,
}

// This is different compared to DriverValue

#[repr(u8)]
#[derive(Debug, strum_macros::Display)]
pub enum MacroKeyType {
	Key = 1,
	Mouse = 2,
}

#[repr(u8)]
pub enum MacroKeyState {
	Down = 1,
	Up = 2,
}

pub enum OpCodes {
	/// Information about the keyboard
	Info = 0x01,

	/// Restarts the keyboard (can reboot in special modes such as "CDBoot")
	RestartKeyboard = 0x03,
	Unk04 = 0x04, // Some diagnostics stuff? Or maybe something related to key input? Or macros ("KeyPress")?

	/// Set the active layer (base / driver / 1 / 2 / 3)
	SetLayer = 0x0B,

	/// Ping / keep alive
	Ping = 0x0C,

	/// Using macros when in the "driver" layer
	DriverMacro = 0x15,

	/// Set "driver" layer key values
	DriverLayerSetKeyValues = 0x16,

	/// Set "driver" layer config values (seem to be hard coded in the application)
	DriverLayerSetConfig = 0x17,

	/// The keyboard sends this packet to enable macros/shortcut/keypress lighting in the "driver" layer (the PC doesn't have to send request)
	DriverKeyCallback = 0x18,

	/// Updates the lighting in real time when in the "driver" layer
	DriverLayerUpdateRealtimeLighting = 0x1A,

	/// Resets a type of data (keys, lights, etc) for a layer
	LayerResetDataType = 0x21,
	LayerSetKeyValues = 0x22,
	Unk23KbData = 0x23,         // Likely a keyboard data set (see KeyboardLayerDataType)
	Unk24KbDataLighting = 0x24, // Some lighting related data (see KeyboardLayerDataType)
	LayerSetMacros = 0x25,

	/// Sets the lighting effects which should play when pressing keys ("Press Light")
	LayerSetKeyPressLightingEffect = 0x26,
	LayerSetLightValues = 0x27,

	/// Function key values
	LayerFnSetKeyValues = 0x31,
}

#[repr(u8)]
pub enum OpCodesSetDriverLayerKeyValues {
	Set = 1,
	SetFn = 2,

	/// Lighting effects which should play when pressing keys
	PressLightingEffect = 3,
}

#[repr(u8)]
pub enum OpCodesDriverLayerUpdateRealtimeLighting {
	Update = 1,
	UpdateComplete = 2,
}

#[repr(u8)]
pub enum OpCodesDriverMacro {
	MouseState = 1,
	KeyboardState = 2,
	BeginEnd = 3, // TODO: Think of a better name (my mind is blank!)
}

pub enum OpCodesInfo {
	/// The firmware id / version
	FirmwareId = 0x01,

	/// Unknown value (-1). This comes with crc validation, and always fails due to the value being -1.
	/// This is likely always -1 for the lifetime of the board (or until firmware change) based on the disassembly.
	Unk02 = 0x02,

	/// The model id of the keyboard
	ModelId = 0x08,

	/// Some kind of buffer size info related to holding keyboard data
	InitBuffers = 0x09,
}

#[repr(u8)]
#[derive(Debug, Hash, Eq, PartialEq, Clone, Copy)]
pub enum KeyboardLayer {
	Base = 1,
	Layer1 = 2,
	Layer2 = 3,
	Layer3 = 4,

	/// A better name for this would be "App"/"Software", as this mode only works when connected to the software.
	Driver = 5,
}

/// Types of configurable data that can be sent to the keyboard (keys, lights, macro, etc)
/// This is used by "21 XX"
#[repr(u8)]
pub enum KeyboardLayerDataType {
	Invalid,
	KeySet = 1, // Maps to 22 XX

	// Where/what is type 2? It could possibly be "23 XX", there seems to be a handler for this on the keyboard
	/// "Lighting effect"? Some sort of lighting related data, but not any of the lighting options I've seen so far
	/// (always 0x210 / 528 bytes of data - which is the same as the amount of data "driver" layer realtime lighting sends)
	LEData = 3, // Maps to 24 XX
	Macros = 4, // Maps to 25 XX

	/// Lighting effects which should play when pressing keys
	KeyPressLightingEffect = 5, //Maps to 26 XX

	/// Lighting data
	Lighting = 6, // Maps to 27 XX
	FnKeySet = 7, // Maps to 31 XX
}

#[repr(u16)]
pub enum OpCodesInfo2 {
	/// The firmware id / version
	FirmwareId = 0x0101,

	/// Unknown value (-1). This comes with crc validation, and always fails due to the value being -1.
	/// This is likely always -1 for the lifetime of the board (or until firmware change) based on the disassembly.
	Unk02 = 0x0201,

	/// The model id of the keyboard
	ModelId = 0x0801,

	/// Some kind of buffer size info related to holding keyboard data
	InitBuffers = 0x0901,
}
