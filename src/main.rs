#![warn(clippy::all, clippy::pedantic, clippy::nursery, clippy::cargo)]
#![allow(clippy::unreadable_literal, clippy::module_name_repetitions, clippy::upper_case_acronyms, dead_code, clippy::single_match_else)]
//#![warn(missing_docs)]
#![feature(thread_id_value)]
#![feature(default_free_fn)]
#![feature(duration_consts_2)]
#![forbid(unsafe_code)]

use std::{
	net::TcpListener,
	str::FromStr,
	sync::{
		atomic::{AtomicBool, Ordering},
		Arc,
	},
	thread::sleep,
};

use crossbeam::thread;
use rusb::Context;
use structopt::StructOpt;
use thiserror::Error;
use tracing::{debug, Level};
use tracing_subscriber::{util::SubscriberInitExt, FmtSubscriber};

use keyboard::device_manager::KeyboardManager;
use uni::Error;
use util::{
	version::{TYPE, VERSION},
	DUR,
};

//mod app;
mod keyboard;
mod mapping;
mod uni;
mod util;

#[derive(StructOpt)]
struct Args {
	/// Main Path
	#[structopt(short = "mp", long = "mainpath", default_value = "TUI")]
	mp: MP,
	#[structopt(short = "d", long = "debug")]
	debug: bool,
}

#[repr(u8)]
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum ExitCode {
	#[error("The application is already running!")]
	AlreadyRunning = 5,
}

#[inline]
fn create_app_lock() -> TcpListener {
	match TcpListener::bind(("127.241.176.205", 31932)) {
		Ok(socket) => {
			debug!("Added lock on instance!");
			socket
		}
		Err(_) => {
			eprintln!("{}", ExitCode::AlreadyRunning);
			std::process::exit(ExitCode::AlreadyRunning as i32);
		}
	}
}

#[inline]
fn remove_app_lock(socket: TcpListener) {
	drop(socket);
	debug!("Removed lock on instance!");
}

#[derive(PartialEq)]
enum MP {
	#[cfg(feature = "gui")]
	GUI,
	TUI,
	CLI,
}

impl FromStr for MP {
	type Err = Error;
	fn from_str(s: &str) -> Result<Self, Error> {
		Ok(match str::to_lowercase(s).as_str() {
			#[cfg(feature = "gui")]
			"gui" => Self::GUI,
			"cli" => Self::CLI,
			"tui" => Self::TUI,
			_ => return Err(Error::FailedToParseMPFromStr),
		})
	}
}

#[paw::main]
fn main(args: Args) {
	let subscriber_debug = FmtSubscriber::builder().with_target(false).with_max_level(Level::DEBUG).finish();
	let subscriber_info = FmtSubscriber::builder().without_time().with_target(false).with_max_level(Level::INFO).finish();

	#[cfg(not(feature = "gui"))]
	if args.mp == MP::CLI {
		if args.debug {
			subscriber_debug.init();
		} else {
			subscriber_info.init();
		}
	}

	#[cfg(feature = "gui")]
	if args.mp == MP::GUI || args.mp == MP::CLI {
		if args.debug {
			subscriber_debug.init();
		} else {
			subscriber_info.init();
		}
	}

	let lock_socket = create_app_lock();

	println!(
		"GK6X RS v{app_version}{type_version}! built with Rust {rust_version}!",
		app_version = VERSION,
		type_version = TYPE,
		rust_version = rustc_version_runtime::version(),
	);
	println!("Space. The final frontier.");

	let running = Arc::new(AtomicBool::new(true));
	let r = running.clone();

	ctrlc::set_handler(move || {
		r.store(false, Ordering::SeqCst);
	})
	.expect("Failed to setup signal handler!");

	{
		let context = Context::new().expect("Fatal Error! The initialization of LibUSB instance failed!"); // FIXME: Refactor THIS!
		let mut keyboard_manager = KeyboardManager::new();
		thread::scope(|scope| {
			keyboard_manager.start_listening(&context, scope).expect("Fatal Error! Failed to start listening thread!");

			// TUI, GUI, CLI
			match args.mp {
				#[cfg(feature = "gui")]
				MP::GUI => {
					// TODO: Tauri GUI Thread.join() when ends continue to stop listening thread etc... also move reference of keyboard_manager into it
				}
				MP::TUI => {
					// TODO: Make tui.rs aka struct App
					//app::tuim::start().expect("Fatal Error! TUI returned error!");
				}
				MP::CLI => {
					while running.load(Ordering::SeqCst) {
						// TODO: Maybe use rustyline for this?
						sleep(DUR); // 0.15 secs
					}
				}
			}

			keyboard_manager.stop_listening().expect("Fatal Error! Failed to stop listening thread!");
		})
		.expect("The thread scope panicked!");
	}
	remove_app_lock(lock_socket);
}
