use std::{
	cmp::min,
	convert::TryInto,
	io::{Read, Write},
};

use anyhow::{bail, Result};
use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use rusb::{Device, DeviceDescriptor, DeviceHandle, TransferType, UsbContext};
use tracing::{debug, info, warn};

use crate::{
	keyboard::{
		endpoints::{Endpoint, Endpoints},
		identifier::Identifier,
		info::DeviceInfo,
		state::State,
	},
	mapping::{
		enums::{KeyboardLayer, KeyboardLayerDataType, OpCodes, OpCodesDriverMacro, OpCodesInfo2, OpCodesSetDriverLayerKeyValues},
		key_values::DriverValue,
	},
	uni::Error,
	util::{
		bithelper::{Byte, Bytes},
		crc16::{get_crc, insert_crc, validate_crc},
		packet::{Packet, LENGTH_PACKET_DATA},
		TIMEOUT,
	},
};

pub struct KeyboardDevice<T: UsbContext> {
	pub identifier: &'static Identifier,
	pub device_info: DeviceInfo,
	pub handle: DeviceHandle<T>,
	pub endpoints: Endpoints,
	pub endpoint: Endpoint,
	pub state: State,
	pub path: u8,
	//pub ping_thread: Option<Sender<ThreadAction>>,
}

impl<T: UsbContext> Drop for KeyboardDevice<T> {
	fn drop(&mut self) {
		// Just ping it before disconnecting?
		if let Err(e) = self.send_ping() {
			let v = e.downcast_ref();
			if v != Some(&rusb::Error::NoDevice) && v != None {
				warn!(
					"[{}] Failed to sent disconnect packet! The keyboard has to be disconnected from the computer to be able to connect to it again!",
					self.path
				);
			}
		} // NOTE: Ignoring error here!

		info!("[{}] Keyboard \"{}\" disconnected!", self.path, self.state.model_name);

		// self.handle.release_interface(self.endpoint.interface).expect("Couldn't release the used interface!");
		// self.handle.clear_halt(self.endpoint.interface).unwrap();
		// self.handle.unconfigure().unwrap();
		// self.handle.attach_kernel_driver(self.endpoints[&TransferType::Bulk].interface);
		// self.handle.reset().unwrap();
	}
}

impl<T: UsbContext> KeyboardDevice<T> {
	pub fn new(descriptor: &DeviceDescriptor, device: &Device<T>, identifier: &'static Identifier, path: u8) -> Result<Self> {
		let mut handle = device.open()?;

		handle.reset()?;

		let endpoints = Endpoint::find(device, descriptor)?;
		let endpoint = endpoints[&TransferType::Interrupt].clone();

		if handle.set_auto_detach_kernel_driver(true).is_err() {
			debug!("[{}] Failed to enable auto Attach/Detach of kernel driver from interface!", path);
		}

		if handle.active_configuration()? != endpoint.config {
			debug!("[{}] Setting active configuration because current active configuration is not same as on the endpoint!", path);

			match handle.set_active_configuration(endpoint.config) {
				Ok(_) => debug!("[{}] Successfully set active configuration!", path),
				Err(v) => debug!("[{}] Failed to set active configuration because: {}! Possibly not a fatal error! Continuing!", path, v), // We can continue because it's not necessary... I think
			};
		}

		handle.claim_interface(endpoint.interface)?;

		handle.set_alternate_setting(endpoint.interface, endpoint.setting)?;

		let v = Self {
			identifier,
			device_info: DeviceInfo::read(&handle, descriptor)?,
			state: KeyboardDevice::handshake(&handle, &endpoint)?,
			endpoints,
			endpoint,
			handle,
			path,
		};

		// v.set_layer(KeyboardLayer::Layer3)?;
		// v.set_layer(KeyboardLayer::Layer2)?;
		// v.set_identify_driver_macros()?;

		info!("[{}] Keyboard \"{}\" FW: {} connected!", path, v.state.model_name, v.state.firmware_string());

		Ok(v)
	}

	fn read(&mut self) -> Result<Bytes> {
		let mut buffer = Bytes::with_capacity(64);
		self.handle.read_interrupt(self.endpoint.input, &mut buffer, TIMEOUT)?;
		Ok(buffer)
	}

	fn read_pass(&mut self, buffer: &mut [Byte]) -> Result<()> {
		self.handle.read_interrupt(self.endpoint.input, buffer, TIMEOUT)?;
		Ok(())
	}

	fn write(&mut self, data: &[Byte]) -> Result<()> {
		self.handle.write_interrupt(self.endpoint.output, data, TIMEOUT)?;
		Ok(())
	}

	#[inline]
	fn send_ping(&mut self) -> Result<Packet> {
		self.write_packet(OpCodes::Ping, 0, None, None)
	}

	fn handshake(handle: &DeviceHandle<T>, endpoint: &Endpoint) -> Result<State> {
		let mut packet = Self::write_simple_packet(handle, endpoint, OpCodesInfo2::InitBuffers as u16)?;

		let buffer_size_a: Byte = packet.read_u8()?;
		let buffer_size_b: Byte = packet.read_u8()?;

		if buffer_size_a == 0 || buffer_size_b == 0 {
			bail!(Error::BufferSize);
		}

		let mut packet = Self::write_simple_packet(handle, endpoint, OpCodesInfo2::FirmwareId as u16)?;

		let firmware_id = packet.read_u32::<LittleEndian>()?;
		let firmware_minor_version = packet.read_u8()?;
		let firmware_major_version = packet.read_u8()?;

		if firmware_id == 0 || (firmware_minor_version == 0 && firmware_major_version == 0) {
			bail!(Error::FirmwareID);
		}

		let mut packet = Self::write_simple_packet(handle, endpoint, OpCodesInfo2::ModelId as u16)?;

		let mut crc_bytes = [0_u8; 6];
		packet.read_exact(&mut crc_bytes)?;
		packet.set_position(8);
		let calculated_model_id_crc = get_crc(&crc_bytes, None, None);
		let model_id = packet.read_u32::<LittleEndian>()?;

		if model_id == 0 {
			bail!(Error::ModelID);
		}

		let _crc_validation1 = packet.read_u16::<LittleEndian>()?;
		let model_id_crc = packet.read_u16::<LittleEndian>()?;

		if calculated_model_id_crc != model_id_crc {
			bail!(Error::ModelCRC);
		}

		// let mut whaaa = Self::write_simple_packet(handle, endpoint, OpCodesInfo2::Unk02 as u16)?;
		// debug!("DATA IN: {:02x?}", whaaa.get_buffer());

		let mut state = State::get_keyboard_state(model_id)?;

		if state.firmware_id != firmware_id {
			bail!(Error::KeebData);
		}

		state.firmware_minor_version = firmware_minor_version;
		state.firmware_major_version = firmware_major_version;
		state.initialize_buffers(buffer_size_a, buffer_size_b)?;
		Ok(state)
	}

	pub fn write_packet(&mut self, op1: OpCodes, op2: Byte, packet: Option<Packet>, op3: Option<Byte>) -> Result<Packet> {
		let mut num_packets = 1;
		let mut offset = 0;

		let mut complete_packet: Packet = match packet {
			Some(v) => {
				num_packets += v.get_ref().len() / LENGTH_PACKET_DATA; // LENGTH_PACKET_DATA == 56
				v
			}
			None => Packet::default(),
		};

		if complete_packet.get_ref().len() < LENGTH_PACKET_DATA * num_packets {
			// dbg!((LENGTH_PACKET_DATA * num_packets) - complete_packet.get_ref().len());
			complete_packet.get_mut().resize(LENGTH_PACKET_DATA * num_packets, 0);
		}

		let complete_buffer = complete_packet.get_buffer();

		let offset_offset = 2;
		let mut length_offset = 4;
		let mut allow_long_offset = false;

		if let OpCodes::DriverLayerSetKeyValues | OpCodes::DriverLayerUpdateRealtimeLighting | OpCodes::LayerSetLightValues = op1 {
			length_offset = 5;
			allow_long_offset = true;
		}

		let op1 = op1 as Byte;
		for i in 0..num_packets {
			let mut report = [0_u8; 64];
			report[0] = op1;
			report[1] = op2;

			// Not really an op, used for setting keyboard data buffers
			if let Some(v) = op3 {
				if v > 0 {
					report[2] = v;
				}
			}

			if !complete_buffer.is_empty() {
				let num_bytes_to_write = match min(LENGTH_PACKET_DATA, complete_buffer.len() - offset).try_into() {
					Ok(v) => v,
					Err(_) => bail!(Error::NumBytesToWriteNotCorrect),
				}; // LENGTH_PACKET_DATA == 56

				report[8..].copy_from_slice(&complete_buffer[offset..offset + num_bytes_to_write as usize]);

				if num_packets > 1 {
					let bytes_offset = offset.to_le_bytes();
					report[offset_offset] = bytes_offset[0];
					report[offset_offset + 1] = bytes_offset[1];
					if allow_long_offset {
						// REVIEW: Missing 	third byte????!
						report[offset_offset + 3] = bytes_offset[4];
					}
					report[length_offset] = num_bytes_to_write;
				}
			}

			insert_crc(&mut report, None, None /*6*/);
			self.write(&report)?;

			let mut result_buffer = self.get_response(op1, op2)?;

			if result_buffer[0] != op1 || result_buffer[1] != op2 {
				bail!(Error::CaughtWrongPacket);
			}

			if !validate_crc(&mut result_buffer, None, None) {
				bail!(Error::CRCValidation);
			}

			if i == num_packets - 1 {
				complete_packet.set_position(8);
				return Ok(complete_packet);
			}

			offset += LENGTH_PACKET_DATA; // LENGTH_PACKET_DATA == 56
		}
		bail!(Error::NoResponse)
	}

	fn get_response(&mut self, op1: Byte, _op2: Byte) -> Result<[u8; 64]> {
		// This is a bit of a hack to consume any packets sent from the keyboard which we didn't request
		// such as changing the keyboard layer manually. TODO: Add a proper packet handler and remove this hack.
		loop {
			let mut result_buffer = [0; 64];
			self.read_pass(&mut result_buffer)?;

			if result_buffer[0] == op1 {
				return Ok(result_buffer);
			}

			if result_buffer[0] == OpCodes::DriverKeyCallback as Byte {
				let callback_id = result_buffer[2];
				let callback_key_down = result_buffer[3] != 0;
				if callback_key_down {
					let key = self.state.keys_by_logic_code.get(&i16::from(callback_id));

					match key {
						Some(v) => debug!(
							"Index: {}\nName: {}\nIndex: {}\nLocation: {}\nD: {:#04X}\nE: {:?}\nS: {}",
							callback_id, v.key_name, v.logic_code, v.location_code, v.driver_value as u32, v.driver_value, v.driver_value_name
						),
						None => debug!("Index: {}\n (None)", callback_id),
					}
				}
			} else if !validate_crc(&mut result_buffer, None, None /*6*/) {
				bail!(Error::CRCValidation);
			}
		}
	}

	pub fn write_simple_packet(handle: &DeviceHandle<T>, endpoint: &Endpoint, opcode: u16) -> Result<Packet> {
		let mut packet = Packet::default();
		packet.write_u16::<LittleEndian>(opcode)?;
		packet.write_all(&[0_u8; 62])?;
		let buffer = packet.get_buffer();
		insert_crc(buffer, None, None);

		handle.write_interrupt(endpoint.output, buffer, TIMEOUT)?;
		handle.read_interrupt(endpoint.input, buffer, TIMEOUT)?;

		// All handshake packets should have a result code of 1
		if buffer[2] != 1 || buffer[0..=1] != opcode.to_le_bytes() {
			bail!(Error::HandshakeResultCode);
		}

		if !validate_crc(buffer, None, None) {
			bail!(Error::CRCValidation);
		}

		packet.set_position(8);
		Ok(packet)
	}

	pub fn set_layer(&mut self, layer: KeyboardLayer) -> Result<()> {
		self.write_packet(OpCodes::SetLayer, layer as Byte, None, None)?;
		Ok(())
	}

	pub fn set_keys(&mut self, layer: KeyboardLayer, values: &[DriverValue], fnk: bool) -> Result<()> {
		self.write_packet(
			OpCodes::LayerResetDataType,
			layer as Byte,
			None,
			Some(if fnk { KeyboardLayerDataType::FnKeySet as Byte } else { KeyboardLayerDataType::KeySet as Byte }),
		)?;

		let mut packet = Packet::default();

		for v in values {
			packet.write_u32::<LittleEndian>(*v as u32)?;
		}

		self.write_packet(if fnk { OpCodes::LayerFnSetKeyValues } else { OpCodes::LayerSetKeyValues }, layer as Byte, Some(packet), None)?;

		Ok(())
	}

	#[allow(unused_must_use)]
	pub fn set_identify_driver_macros(&mut self) -> Result<()> {
		let mut packet = Packet::default();
		for i in 0..255 {
			packet.write_u32::<LittleEndian>(0x0A010000 | i)?;
		}
		self.write_packet(OpCodes::DriverLayerSetKeyValues, OpCodesSetDriverLayerKeyValues::Set as Byte, Some(packet), None);

		packet = Packet::default();
		packet.write_u8(1)?;
		self.write_packet(OpCodes::DriverMacro, OpCodesDriverMacro::BeginEnd as Byte, Some(packet), None);

		packet = Packet::default();
		packet.get_mut().resize(256, 0);
		// packet.write_all(&[0_u8; 256])?;
		self.write_packet(OpCodes::DriverMacro, OpCodesDriverMacro::KeyboardState as Byte, Some(packet), None);

		// packet = Packet::default();
		// packet.write_u8(0)?;
		self.write_packet(OpCodes::DriverMacro, OpCodesDriverMacro::BeginEnd as Byte, None, None);

		Ok(())
	}
}
