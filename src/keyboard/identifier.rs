use rusb::DeviceDescriptor;

use crate::uni::Error;

const SUPPORTED_DEVICES: &[Identifier] = &[
	Identifier { vendor_id: 0x1EA7, product_id: 0x907 },
	Identifier { vendor_id: 0x1EA7, product_id: 0x9005 },
	Identifier { vendor_id: 0x1EA7, product_id: 0x9018 },
];

pub struct Identifier {
	pub vendor_id: u16,
	pub product_id: u16,
}

impl Identifier {
	pub fn is_supported<'a>(descriptor: &'a DeviceDescriptor) -> Result<&'static Self, Error> {
		let vendor_id = descriptor.vendor_id();
		let product_id = descriptor.product_id();

		match SUPPORTED_DEVICES.iter().find(|i| i.vendor_id == vendor_id && i.product_id == product_id) {
			Some(a) => Ok(a),
			None => Err(Error::NotSupported(vendor_id, product_id)),
		}
	}
}
