use std::collections::HashMap;

use anyhow::{bail, Result};
use rusb::{Device, DeviceDescriptor, Direction, InterfaceDescriptor, TransferType, UsbContext};
use smallvec::SmallVec;

use crate::uni::Error;

pub type Endpoints = HashMap<TransferType, Endpoint>;

#[derive(Clone)]
pub struct Endpoint {
	pub interface: u8,
	pub config: u8,
	pub setting: u8,
	pub input: u8,
	pub output: u8,
}

impl Endpoint {
	pub fn find<T: UsbContext>(device: &Device<T>, descriptor: &DeviceDescriptor) -> Result<Endpoints> {
		let mut endpoints = Endpoints::new();

		for config in 0..descriptor.num_configurations() {
			let config_descriptor = device.config_descriptor(config)?;
			let config = config_descriptor.number();

			let interfaces: SmallVec<[InterfaceDescriptor; 5]> = config_descriptor.interfaces().flat_map(|i| i.descriptors()).collect();

			for interface in interfaces {
				let interface_number = interface.interface_number();
				let setting_number = interface.setting_number();
				let mut input = None;
				let mut output = None;
				let mut transfer_type = None;

				for endpoint_descriptor in interface.endpoint_descriptors() {
					match endpoint_descriptor.direction() {
						Direction::In => input = Some(endpoint_descriptor.address()),
						Direction::Out => output = Some(endpoint_descriptor.address()),
					}
					transfer_type = Some(endpoint_descriptor.transfer_type());
				}

				if let (Some(transfer_type), Some(input), Some(output)) = (transfer_type, input, output) {
					endpoints.insert(
						transfer_type,
						Self {
							interface: interface_number,
							setting: setting_number,
							config,
							input,
							output,
						},
					);
				}
			}
		}

		if endpoints.is_empty() || !endpoints.contains_key(&TransferType::Interrupt) {
			bail!(Error::NoProperEndpointFound)
		}

		Ok(endpoints)
	}
}
