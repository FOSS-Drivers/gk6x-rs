use rusb::{DeviceDescriptor, DeviceHandle, Error, Result, UsbContext};

use crate::util::TIMEOUT;

#[derive(Debug)]
pub struct DeviceInfo {
	pub manufacturer: String,
	pub name: String,
	pub serial: String,
}

impl DeviceInfo {
	pub fn read<T: UsbContext>(handle: &DeviceHandle<T>, descriptor: &DeviceDescriptor) -> Result<Self> {
		let languages = handle.read_languages(TIMEOUT)?;
		let language = match languages.first() {
			Some(value) => *value,
			None => return Err(Error::NotFound),
		};

		let manufacturer = handle.read_manufacturer_string(language, descriptor, TIMEOUT)?;
		let name = handle.read_product_string(language, descriptor, TIMEOUT)?;
		let serial = handle.read_serial_number_string(language, descriptor, TIMEOUT)?;

		Ok(Self { manufacturer, name, serial })
	}
}
