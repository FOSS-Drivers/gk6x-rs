use std::{collections::HashMap, fmt, fs, sync::Mutex};

use anyhow::{bail, Result};
use itertools::{repeat_n, Itertools};
use lazy_static::lazy_static;
use num::FromPrimitive;
use serde::{Deserialize, Deserializer, Serialize};
use serde_json::Value;
use tracing::debug;

use key_values::DriverValue;

use crate::{
	mapping::{
		enums::KeyboardLayer,
		key_values::{self, KEYS_BY_LOGIC_CODE},
	},
	uni::Error,
	util::{bithelper::Byte, DATA_BASE_PATH, MODEL_LIST_PATH},
};

lazy_static! {
	pub static ref KEYBOARD_STATES_BY_MODEL_ID: Mutex<HashMap<u32, State>> = Mutex::new(HashMap::new());
	pub static ref MODELS: Vec<State> = {
		let contents = fs::read_to_string(&*MODEL_LIST_PATH).unwrap();
		serde_json::from_str(&contents).expect("Fatal Error! Failed to load Keyboard models from the disk!")
	};
}

// REVIEW: Theoretically buffer_size and firmware_version fields should be Options, but in reality it's not needed because it's never called before the function that gets ir or errors out
#[derive(Serialize, Deserialize, Clone)]
pub struct State {
	#[serde(skip)]
	pub buffer_size_a: Byte,
	#[serde(skip)]
	pub buffer_size_b: Byte,

	#[serde(skip)]
	pub firmware_major_version: Byte,
	#[serde(skip)]
	pub firmware_minor_version: Byte,
	#[serde(skip)]
	pub keys_by_driver_value: HashMap<DriverValue, KeyJS>,
	/// A unique name for every key. Keys with duplicate DriverValue entries will be given separate names here.
	#[serde(skip)]
	pub keys_by_driver_value_name: HashMap<String, KeyJS>,
	#[serde(skip)]
	pub keys_by_location_code: HashMap<i16, KeyJS>,
	#[serde(skip)]
	pub keys_by_logic_code: HashMap<i16, KeyJS>,
	#[serde(skip)]
	pub layers: HashMap<KeyboardLayer, KeyboardStateLayer>,

	#[serde(rename = "FWID")]
	#[serde(deserialize_with = "hex_numstring_to_num")]
	pub firmware_id: u32,
	#[serde(rename = "ModelID")]
	pub model_id: u32,
	#[serde(rename = "Name")]
	pub model_name: String,
}

fn hex_numstring_to_num<'de, D: Deserializer<'de>>(deserializer: D) -> Result<u32, D::Error> {
	let s: String = Deserialize::deserialize(deserializer)?;

	// Be careful! The hex number sometimes contains ending whitespace!! Traitor!!
	return Ok(match u32::from_str_radix(s.trim_start_matches("0x").trim_end(), 16) {
		Ok(v) => v,
		Err(_) => {
			debug!("Failed to convert \"{}\" to number!", s);
			return Err(serde::de::Error::custom("Failed to parse Hex Number String to Number!"));
		}
	});
}

impl State {
	pub fn firmware_version_u16(&self) -> u16 {
		(u16::from(self.firmware_major_version) << 8) | u16::from(self.firmware_minor_version)
	}

	pub fn firmware_version_str(&self, mut f: fmt::Formatter) -> fmt::Result {
		write!(f, "v{}.{}", self.firmware_major_version, self.firmware_minor_version)
	}

	/// The maximum value for a `LogicCode` understood by the keyboard.
	/// NOTE: Some logic codes are unused within this range.
	fn max_logic_code(&self) -> u16 {
		u16::from(self.buffer_size_a) * u16::from(self.buffer_size_b)
	}

	fn has_initialized_buffers(&self) -> bool {
		self.max_logic_code() > 0
	}

	pub fn firmware_string(&self) -> String {
		format!("v{}.{}", self.firmware_major_version, self.firmware_minor_version)
	}

	// pub fn get_keyboard_state(&self) -> Result<Self, Error> {
	//     match KEYBOARD_STATES_BY_MODEL_ID.lock().unwrap().get(&self.model_id) {
	//         Some(a) => Ok(a.clone()),
	//         None => Err(Error::KeyboardStateNotFound(self.model_id)),
	//     }
	// }

	pub fn initialize_buffers(&mut self, size_a: Byte, size_b: Byte) -> Result<()> {
		if !self.layers.is_empty() {
			self.layers.clear();
		}

		if self.firmware_id == 0 || self.model_id == 0 {
			self.buffer_size_a = 0;
			self.buffer_size_b = 0;
			bail!(Error::InitBuffer);
		};

		self.buffer_size_a = size_a;
		self.buffer_size_b = size_b;

		self.create_factory_default_layers()?;
		Ok(())
	}

	fn create_factory_default_layers(&mut self) -> Result<()> {
		self.create_layer(KeyboardLayer::Base, None)?;
		self.create_layer(KeyboardLayer::Driver, None)?;
		self.create_layer(KeyboardLayer::Layer1, None)?;
		self.create_layer(KeyboardLayer::Layer2, None)?;
		self.create_layer(KeyboardLayer::Layer3, None)?;
		Ok(())
	}

	fn create_layer(&mut self, layer: KeyboardLayer, mut model_data: Option<HashMap<String, Value>>) -> Result<()> {
		let mut state = KeyboardStateLayer::default();

		let profile_layer_name = match layer {
			KeyboardLayer::Base => String::with_capacity(0),
			KeyboardLayer::Driver => String::from("_online_1"),
			KeyboardLayer::Layer1 => String::from("_offline_1"),
			KeyboardLayer::Layer2 => String::from("_offline_2"),
			KeyboardLayer::Layer3 => String::from("_offline_3"),
		};

		let model_data = match model_data {
			Some(v) => v,
			None => {
				if state.factory_default_model_data.is_none() {
					let profile_path = DATA_BASE_PATH
						.join("device")
						.join(self.model_id.to_string())
						.join("data")
						.join(format!("profile{}", profile_layer_name))
						.with_extension("json");

					// Load factory default model data from JSON file by Model ID
					state.factory_default_model_data = Some(
						match {
							let contents = fs::read_to_string(profile_path)?;
							serde_json::from_str(&contents)
						} {
							Ok(v) => v,
							Err(_) => bail!(Error::NoFactoryDefaultData(self.model_id)),
						},
					);
				};

				model_data = state.factory_default_model_data.clone();

				match model_data {
					Some(a) => a,
					None => bail!(Error::NoFactoryDefaultData(self.model_id)),
				}
			}
		};

		state.key_set = repeat_n(DriverValue::UnusedKeyValue, usize::from(self.buffer_size_a) * usize::from(self.buffer_size_b)).collect_vec();
		state.fn_key_set = state.key_set.clone();

		setup_driver_key_set_buffer(&model_data, "KeySet", &mut state.key_set)?;
		if setup_driver_key_set_buffer(&model_data, "FnKeySet", &mut state.fn_key_set).is_err() {
			debug!(
				"Failed to setup \"FnKeySet\" for model \"{}\" in layer \"{:?}\"! Possibly not a fatal error! Continuing!",
				self.model_id, layer
			);
		}; // This is allowed to fail! Not every single keyboard layer has `FnKeySet`!

		// `DeviceLE` can be missing too!! and the process can be continued!
		if let Some(device_le_data) = model_data.get("DeviceLE") {
			if let Ok(v) = serde_json::from_value::<HashMap<String, Value>>(device_le_data.clone()) {
				if v.contains_key("LESet") {
					state.has_le_set = true;
				}
			}
		} else {
			debug!(
				"Failed to get \"DeviceLE\" from layer \"{:?}\" for model \"{}\"! Possibly not a fatal error! Continuing!",
				layer, self.model_id
			);
		}

		self.layers.insert(layer, state);
		Ok(())
	}

	/// Returns a `DriverValue` where each index specifies the `LogicCode`
	fn get_default_profile_driver_values(&self) -> Result<Vec<DriverValue>> {
		let profile_path = DATA_BASE_PATH.join("device").join(self.model_id.to_string()).join("data").join("profile").with_extension("json");

		let model_data: HashMap<String, Value> = {
			let contents = fs::read_to_string(profile_path)?;
			serde_json::from_str(&contents)?
		};

		let mut result = repeat_n(DriverValue::UnusedKeyValue, i16::MAX as usize).collect_vec();
		setup_driver_key_set_buffer(&model_data, "KeySet", &mut result)?;

		Ok(result)
	}

	fn load_keys(&mut self) -> Result<()> {
		let driver_values = match self.get_default_profile_driver_values() {
			Ok(v) => v,
			Err(_) => bail!(Error::DefaultProfileDriverValues),
		};

		let keys_path = DATA_BASE_PATH.join("device").join(self.model_id.to_string()).join("data").join("keymap").with_extension("js");

		let keys: Vec<KeyJS> = {
			let contents = fs::read_to_string(keys_path)?;
			serde_json::from_str(&contents)?
		};

		// TODO: Find a way to make this work without the int overflowing aka losing sign
		for mut key in keys {
			// REVIEW: Very ugly
			if key.logic_code >= 0 && driver_values.get(key.logic_code as usize) != Some(&DriverValue::UnusedKeyValue) && driver_values.get(key.logic_code as usize) != None {
				key.driver_value = driver_values[key.logic_code as usize];
			} else {
				// LogicCode of -1 is assumed to be the Fn key
				if key.logic_code > 0 {
					match KEYS_BY_LOGIC_CODE.get(&i32::from(key.logic_code)) {
						Some(v) => key.driver_value = v.driver_value,
						None => {
							debug!(
								"Couldn't find driver_value for key \"{}\", logic_code: {}, location_code: {}, model_id: {}, model_name: {}",
								key.key_name, key.logic_code, key.location_code, self.model_id, self.model_name
							);
						}
					}
				}
			};

			self.keys_by_location_code.insert(key.location_code, key.clone());
			self.keys_by_logic_code.insert(key.logic_code, key.clone());
			self.keys_by_driver_value.insert(key.driver_value, key.clone());

			let driver_value_name = format!("{}", &key.driver_value);
			if self.keys_by_driver_value_name.insert(driver_value_name.clone(), key.clone()).is_none() {
				key.driver_value_name = driver_value_name;
			};
		}

		Ok(())
	}

	pub fn get_keyboard_state(model_id: u32) -> Result<Self> {
		let mut unlocked = match KEYBOARD_STATES_BY_MODEL_ID.try_lock() {
			Ok(v) => v,
			Err(_) => bail!(Error::KeyboardStatesIsPoisoned),
		};

		Ok(match unlocked.get(&model_id) {
			Some(v) => v.clone(),
			None => {
				let v = match Self::load_keyboard_model(model_id) {
					Ok(v) => v,
					Err(_) => bail!(Error::FailedToLoadKeyboardModel(model_id)),
				};
				unlocked.insert(model_id, v.clone());
				v
			}
		})
	}

	fn load_keyboard_model(model_id: u32) -> Result<Self> {
		debug!("Trying to load state of model \"{}\" from the disk!", model_id);

		let mut state = MODELS.iter().find(|s| s.model_id == model_id).ok_or(Error::FailedToFindModel(model_id))?.clone();

		if state.load_keys().is_err() {
			debug!("Failed to load keys for model \"{}\"! Fatal error! Skipping!", model_id);
			bail!(Error::FailedToLoadKeys);
		};

		debug!("Finished loading state of model \"{}\" from the disk!", model_id);
		Ok(state)
	}
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct KeySetEntry {
	#[serde(rename = "Index")]
	index: u32,
	#[serde(rename = "DriverValue")]
	#[serde(deserialize_with = "hex_numstring_to_num")]
	driver_value: u32,
	#[serde(rename = "MenuName")]
	name: String,
	// #[serde(rename = "LESet")]
	// le_set: Option<Vec<Option<HashMap<String, Value>>>>, // REVIEW: Add proper type
}

// REVIEW: Still pretty piggy style of code, remove the cloning etc...
fn setup_driver_key_set_buffer(model_data: &HashMap<String, Value>, arg: &str, driver_key_set: &mut Vec<DriverValue>) -> Result<()> {
	let keys_set_data: Vec<KeySetEntry> = match serde_json::from_value(
		match model_data.get(arg) {
			Some(a) => a,
			None => bail!(Error::FailedToSetupDriverKeySetBuffers),
		}
		.clone(),
	) {
		Ok(v) => v,
		Err(_) => bail!(Error::FailedToSetupDriverKeySetBuffers),
	};

	for mut key_set in keys_set_data {
		if (key_set.index as usize) < driver_key_set.len() {
			let index = key_set.index;
			let driver_value = &mut key_set.driver_value;
			// 167968769 / 167837697
			if *driver_value == 0x0A03_0001 || *driver_value == 0x0A01_0001 {
				// Not 100% sure if this is exactly what the regular code does
				// Yeah... This seems wrong, TODO, REVIEW, FIXME: check the firmware dump
				*driver_value += index;
			}

			driver_key_set[index as usize] = match FromPrimitive::from_u32(*driver_value) {
				Some(v) => v,
				None => {
					debug!("Invalid driver value \"{}\" - \"{:#0X?}\"!", key_set.name, driver_value);
					continue;
				}
			};
		}
	}
	Ok(())
}

// fn load_keyboard_models() -> Result<HashMap<u32, State>> {
// 	debug!("Loading models from the disk!");
// 	let mut map: HashMap<u32, State> = HashMap::new();

// 	let file = OpenOptions::new().read(true).open(&*MODEL_LIST_PATH)?;

// 	let models: Vec<State> = serde_json::from_reader(file)?;

// 	if models.is_empty() {
// 		bail!(Error::FailedToLoadKeyboardModels);
// 	}

// 	for mut i in models {
// 		if i.load_keys().is_ok() {
// 			map.insert(i.model_id, i);
// 		} else {
// 			debug!("Failed to load keys for model \"{}\"! Possibly not a fatal error! Continuing anyway!", i.model_id);
// 			// So anyway I started blasting
// 			continue;
// 		};
// 	}

// 	debug!("Finished loading models from the disk!");
// 	Ok(map)
// }

#[derive(Clone, Serialize, Deserialize)]
pub struct KeyJS {
	#[serde(skip)]
	pub driver_value: DriverValue,
	#[serde(skip)]
	pub driver_value_name: String,
	#[serde(rename = "KeyName")]
	pub key_name: String,
	#[serde(rename = "LocationCode")]
	pub location_code: i16,
	#[serde(rename = "LogicCode")]
	pub logic_code: i16,
	#[serde(rename = "Position")]
	pub position: KeyRect,
	#[serde(rename = "Show")]
	pub show: String,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct KeyRect {
	#[serde(rename = "Left")]
	left: i16,
	#[serde(rename = "Top")]
	top: i16,
	#[serde(rename = "Width")]
	width: i16,
	#[serde(rename = "Height")]
	height: i16,
}

#[derive(Clone)]
pub struct KeyboardStateLayer {
	factory_default_model_data: Option<HashMap<String, Value>>,
	fn_key_set: Vec<DriverValue>,
	/// Only used for "driver" layer? (17 01)
	has_le_set: bool,
	key_press_lightning_effect: [u8; 128],
	key_set: Vec<DriverValue>,
}

impl Default for KeyboardStateLayer {
	fn default() -> Self {
		Self {
			factory_default_model_data: None,
			fn_key_set: Vec::new(),
			has_le_set: false,
			key_press_lightning_effect: [0xFF; 128],
			key_set: Vec::new(),
		}
	}
}
