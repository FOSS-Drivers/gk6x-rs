use std::{collections::HashSet, sync::Arc, thread, time::Duration};

use anyhow::{bail, Result};
use dashmap::DashMap;
use flume::Sender;
use rusb::UsbContext;
use smallvec::SmallVec;
use tracing::{debug, error, info};

use crate::{
	keyboard::{device::KeyboardDevice, identifier::Identifier},
	uni::Error,
	util::DUR,
};

#[repr(u8)]
#[non_exhaustive]
pub enum ThreadAction {
	Terminate,
}

pub struct KeyboardManager<T: UsbContext> {
	pub keyboards: Arc<DashMap<u8, KeyboardDevice<T>>>,
	pub listen_thread: Option<Sender<ThreadAction>>,
}

// (&String, DeviceInfo)
impl<T: UsbContext> KeyboardManager<T> {
	pub fn new() -> Self {
		Self {
			keyboards: Arc::new(DashMap::new()),
			listen_thread: None,
		}
	}

	/// Start listening thread
	pub fn start_listening<'a>(&mut self, api: &'a T, scope: &crossbeam::thread::Scope<'a>) -> Result<()> {
		if self.listen_thread.is_some() {
			bail!(Error::FailedToStartListeningThread);
		}

		let keyboards = Arc::clone(&self.keyboards);
		let (tx, rx) = flume::unbounded();

		scope.builder().name(String::from("Listening Thread")).spawn(move |_| {
			let thread_id = thread::current().id().as_u64();
			debug!("Listening thread[ID: {}] has started!", thread_id);

			let mut block_list = HashSet::new();

			loop {
				{
					// Temp path of valid devices
					let mut temp_path_devices: SmallVec<[u8; 10]> = SmallVec::new();

					for hw_device in match api.devices() {
						Ok(v) => v,
						Err(_) => {
							error!("Failed to get list of connected devices! Repeating in 3 seconds!");
							thread::sleep(Duration::from_secs(3));
							continue;
						}
					}
					.iter()
					{
						let path = hw_device.address();

						if block_list.contains(&path) {
							continue;
						}

						let descriptor = match hw_device.device_descriptor() {
							Ok(v) => v,
							Err(v) => {
								block_list.insert(path);
								error!("[{}] Failed to get Device Descriptor because: {}! Added to instance blocklist! Skipping!", path, v);
								continue;
							}
						};

						let identifier = match Identifier::is_supported(&descriptor) {
							Ok(v) => v,
							Err(_) => {
								block_list.insert(path);
								// This is going to happen so often, we're not going to write this out!
								//debug!("The device [{:04x}:{:04x}] is not supported!", descriptor.vendor_id(), descriptor.product_id());
								continue;
							}
						};

						temp_path_devices.push(path);

						if keyboards.contains_key(&path) {
							continue;
						}

						info!("[{}] Connecting keyboard!", path);

						let keyboard = match KeyboardDevice::new(&descriptor, &hw_device, identifier, path) {
							Ok(v) => v,
							Err(e) => {
								block_list.insert(path);
								error!("[{}] Couldn't initialize Keyboard because: {}! Added to instance blacklist! Skipping!", path, e);
								continue;
							}
						};

						keyboards.insert(path, keyboard);
					}

					keyboards.retain(|x, _| temp_path_devices.contains(x));
				}

				thread::sleep(DUR); // 0.15 seconds

				if let Ok(v) = &rx.try_recv() {
					match v {
						ThreadAction::Terminate => {
							debug!("Listening thread[ID: {}] is shutting down!", thread_id);
							break;
						}
					}
				}
			}
		})?;

		self.listen_thread = Some(tx);

		Ok(())
	}

	/// Stop listening thread
	pub fn stop_listening(&mut self) -> Result<(), Error> {
		if let Some(v) = &self.listen_thread {
			if v.try_send(ThreadAction::Terminate).is_err() {
				return Err(Error::FailedToStopListeningThread);
			}
		}

		self.listen_thread = None;
		Ok(())
	}

	/// [Inline] is listening?
	#[inline]
	pub fn is_listening(&self) -> bool {
		self.listen_thread.is_some()
	}
}
