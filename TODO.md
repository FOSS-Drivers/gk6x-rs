* [ ] Auto update of Keeb Datas
* [ ] Firmware Update (This will be Experimental forever)
* [ ] Overall Refactor & Reformat
    - [ ] Clean int variations
    - [ ] Rewrite functions that look just so wrong => REVIEW ANCHOR
* [ ] Make human readable list of supported devices later in the development!
* [ ] Find a way how to init Kemove and GamaKay keyboards, they seem to have specific init, as the OUT Descriptor is hidden or something
* [ ] Try to add Bluetooth support
* [ ] Make the LString to load the sring on each lookup? Not sure about that yet
* [ ] Rename most enums in enums.rs
* [x] Add settings
* [ ] Convert the software to ECS design! (Working on this! I learned this too late :/ *sad trans noises*)
* [ ] Create human interfaces (CLI: Our own implementation?, TUI: Using "tui" crate with "crossterm" as backend, GUI: Tauri?)
* [x] Make lazy loading of keyboard states!
* [x] Rewrite Crc16! http://mdfs.net/Info/Comp/Comms/CRC16.htm

* [NOT MAKING] Custom Firmware (After creating our own Firmware and it will be stable we could make Firmware Update feature stable)
    - NOTE: I won't be able to make compatible firmware with this
    - Easier will be to already just design own PCB with some pretty ARM chip, fuck this /public/rabbit_hole.md
